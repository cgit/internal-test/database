--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Drop databases
--

DROP DATABASE testsrv;




--
-- Drop roles
--

DROP ROLE admin;
DROP ROLE postgres;


--
-- Roles
--

CREATE ROLE admin;
ALTER ROLE admin WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'md5432a32c4af5d6f1d72239b1cef0e107e';
CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'md568b7fd8e2fa28fcf0831046f3cd8d1f7';






--
-- Database creation
--

REVOKE ALL ON DATABASE template1 FROM PUBLIC;
REVOKE ALL ON DATABASE template1 FROM postgres;
GRANT ALL ON DATABASE template1 TO postgres;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;
CREATE DATABASE testsrv WITH TEMPLATE = template0 OWNER = postgres;


\connect postgres

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.14
-- Dumped by pg_dump version 9.5.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- Name: clear_set_test(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.clear_set_test() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
	UPDATE set_test_200 SET testid_ctl = NULL;
	UPDATE set_test_200 SET boarduuid = NULL;
	UPDATE set_test_200 SET current_test = NULL;
	UPDATE set_test_200 SET status = NULL;
	RETURN TRUE;
END;$$;


ALTER FUNCTION public.clear_set_test() OWNER TO postgres;

--
-- Name: update_set_test_row(integer, integer, uuid, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.update_set_test_row(nstation integer, tbid integer, bid uuid, ctest character varying, final_st character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
BEGIN
	IF final_st = 'FINISH' THEN
    	UPDATE public.set_test_200 SET testid_ctl = tbid, boarduuid = bid, current_test = ctest WHERE station = nstation;
	ELSE
	UPDATE public.set_test_200 SET testid_ctl = tbid, boarduuid = bid, current_test = ctest, status = final_st WHERE station = nstation;
	END IF;
END;$$;


ALTER FUNCTION public.update_set_test_row(nstation integer, tbid integer, bid uuid, ctest character varying, final_st character varying) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: hello; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.hello (
    station integer NOT NULL,
    status character varying(40),
    current_test character varying(40),
    final_result integer
);


ALTER TABLE public.hello OWNER TO postgres;

--
-- Name: hello_station_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hello_station_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hello_station_seq OWNER TO postgres;

--
-- Name: hello_station_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.hello_station_seq OWNED BY public.hello.station;


--
-- Name: pruebas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pruebas (
    station integer NOT NULL
);


ALTER TABLE public.pruebas OWNER TO postgres;

--
-- Name: set_test_200; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.set_test_200 (
    station integer NOT NULL,
    testid_ctl integer,
    boarduuid uuid,
    current_test character varying,
    status character varying
);


ALTER TABLE public.set_test_200 OWNER TO postgres;

--
-- Name: set_test_control; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.set_test_control (
    "Station" integer NOT NULL,
    "Status" text,
    "Board" text,
    "Current Test" text,
    "Final Result" integer
);


ALTER TABLE public.set_test_control OWNER TO postgres;

--
-- Name: station; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hello ALTER COLUMN station SET DEFAULT nextval('public.hello_station_seq'::regclass);


--
-- Data for Name: hello; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.hello (station, status, current_test, final_result) FROM stdin;
1	\N	\N	\N
2	\N	\N	\N
3	\N	\N	\N
4	\N	\N	\N
5	\N	\N	\N
6	\N	\N	\N
7	\N	\N	\N
8	\N	\N	\N
9	\N	\N	\N
10	\N	\N	\N
11	\N	\N	\N
12	\N	\N	\N
13	\N	\N	\N
14	\N	\N	\N
\.


--
-- Name: hello_station_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hello_station_seq', 14, true);


--
-- Data for Name: pruebas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pruebas (station) FROM stdin;
\.


--
-- Data for Name: set_test_200; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.set_test_200 (station, testid_ctl, boarduuid, current_test, status) FROM stdin;
1	101	98674c20-795d-11e8-9a8f-e644f56b8aaa	FLASH	RUNNING
2	777	35dd000a-c58d-11e8-8375-e644f56b8edd	END	RUNNING
3	777	35dd000a-c58d-11e8-8375-e644f56b8edd	END	HEY
\.


--
-- Data for Name: set_test_control; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.set_test_control ("Station", "Status", "Board", "Current Test", "Final Result") FROM stdin;
1	Running	IGEP SMARC iMX6DL	MMC	2
2	Finished	IGEP SMARC iMX6DL	End	1
3	Finished	IGEP SMARC iMX6DL	End	1
4	Running	IGEP SMARC iMX6DL	HDMI	2
5	Connected	IGEP SMARC iMX6DL	Start	3
6	Connected	IGEP SMARC iMX6DL	Start	3
7	Finished	IGEP SMARC iMX6DL	End	0
8	Disconnected	IGEP SMARC iMX6DL	Start	4
9	Connected	IGEP SMARC iMX6DL	Start	3
10	Finished	IGEP SMARC iMX6DL	End	1
\.


--
-- Name: hello_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hello
    ADD CONSTRAINT hello_pkey PRIMARY KEY (station);


--
-- Name: set_test_200_2_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.set_test_200
    ADD CONSTRAINT set_test_200_2_pkey PRIMARY KEY (station);


--
-- Name: set_test_control_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.set_test_control
    ADD CONSTRAINT set_test_control_pkey PRIMARY KEY ("Station");


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: TABLE set_test_control; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE public.set_test_control FROM PUBLIC;
REVOKE ALL ON TABLE public.set_test_control FROM postgres;
GRANT ALL ON TABLE public.set_test_control TO postgres;
GRANT ALL ON TABLE public.set_test_control TO PUBLIC;


--
-- PostgreSQL database dump complete
--

\connect template1

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.14
-- Dumped by pg_dump version 9.5.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

\connect testsrv

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.14
-- Dumped by pg_dump version 9.5.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: isee; Type: SCHEMA; Schema: -; Owner: admin
--

CREATE SCHEMA isee;


ALTER SCHEMA isee OWNER TO admin;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: add_test_to_batch(uuid, integer, integer, boolean, oid); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.add_test_to_batch(bid uuid, testid integer, exec_id integer, res boolean, dat oid) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
    a integer;
    t varchar;
BEGIN
     -- Remove this test test from test list
     t := 'test_left_' || exec_id;
     EXECUTE 'DELETE FROM ' || t || ' WHERE testid = ' || testid;
     -- INSERT Test Result
     INSERT INTO board_test (testid, board_uuid, result, external_data, testid_ctl) 
            VALUES (testid, bid, res, dat, exec_id);
     -- Count number of test remain in the list
     SELECT COUNT(*) INTO a FROM t;
     -- Check if we've more test to do
     IF a = 0 THEN
     	-- Not have more test to do
        PERFORM isee.update_testbatch(bid, exec_id, "TERMINATED");
     ELSE
     	-- left more test
        PERFORM isee.update_testbatch(bid, exec_id, "RUN");
     END IF;
     RETURN true;              
END;
$$;


ALTER FUNCTION isee.add_test_to_batch(bid uuid, testid integer, exec_id integer, res boolean, dat oid) OWNER TO admin;

--
-- Name: add_test_to_batch_c(uuid, character varying, integer, boolean, character varying, character varying); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.add_test_to_batch_c(bid uuid, test_name character varying, exec_id integer, res boolean, groupid character varying, dat character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
    a integer;
    t varchar;
    t_num integer;
    t_id integer;
    
BEGIN
     -- Remove this test test from test list
     t := 'test_left_' || exec_id;
     SELECT testdefid INTO t_num FROM isee.test_definition WHERE testdefname = test_name;
     SELECT testid INTO t_id FROM isee.board_test_def WHERE testdefid = t_num  AND testgroupid = groupid;  	
     EXECUTE 'DELETE FROM ' || t || ' WHERE testid = ' || t_id;     
     -- INSERT Test Result
     INSERT INTO isee.board_test (testid, board_uuid, result, external_data, testid_ctl) 
            VALUES (t_id, bid, res, dat, exec_id);
     -- Count number of test remain in the list
     EXECUTE 'SELECT COUNT(*) FROM ' || t INTO a;
     -- Check if we've more test to do
     IF a = 0 THEN
     	-- Not have more test to do
        PERFORM isee.update_testbatch(bid, exec_id, 'TERMINATED');
     ELSE
     	-- left more test
        PERFORM isee.update_testbatch(bid, exec_id, 'RUN');
     END IF;
     RETURN true;              
END;
$$;


ALTER FUNCTION isee.add_test_to_batch_c(bid uuid, test_name character varying, exec_id integer, res boolean, groupid character varying, dat character varying) OWNER TO admin;

--
-- Name: add_test_to_group(character varying, character varying, character varying); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.add_test_to_group(tgid character varying, testname character varying, parameters character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE    
    a integer;
BEGIN
     SELECT mid INTO a FROM isee.board_model WHERE testgroupid = tgid;
     IF NOT FOUND THEN
        -- Not Test found
        RETURN FALSE;
     END IF;	
     SELECT testdefid INTO a FROM isee.test_definition WHERE testdefname = testname;
     IF NOT FOUND THEN
        -- Not Test found
        RETURN FALSE;
     END IF;
     BEGIN  
     	INSERT INTO isee.board_test_def (testgroupid, testdefid, testparam) VALUES (tgid, a, parameters);
     EXCEPTION WHEN unique_violation THEN
     	RETURN FALSE;
     END;
     RETURN TRUE;
END;
$$;


ALTER FUNCTION isee.add_test_to_group(tgid character varying, testname character varying, parameters character varying) OWNER TO admin;

--
-- Name: check_test_ok(integer); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.check_test_ok(test_ctl_id integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
    -- view_name varchar;
    a varchar[];	
    t character varying;
    c record;
BEGIN
     FOR c IN SELECT * FROM isee.board_test WHERE testid_ctl = test_ctl_id LOOP
         IF c.result = FALSE THEN
		t := 'SELECT ARRAY (SELECT testdefname FROM isee.test_definition WHERE testdefid IN
		(SELECT testdefid FROM isee.board_test_def WHERE testid IN 
		(SELECT testid FROM isee.board_test WHERE (testid_ctl = ' || test_ctl_id || ' AND result = FALSE))))';
		EXECUTE t INTO a;
		UPDATE isee.set_test SET current_test = a WHERE testid_ctl = test_ctl_id; 
         	-- We've a test failed
            RETURN FALSE;
         END IF;
     END LOOP;
     -- Test OK
     RETURN TRUE;                   
END;
$$;


ALTER FUNCTION isee.check_test_ok(test_ctl_id integer) OWNER TO admin;

--
-- Name: clear_set_test(); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.clear_set_test() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
	-- Destroy Table with Test cases left     
	--SELECT 'DROP Table [' + Table_Schema + '].[' + Table_Name + ']' 
	--FROM   Information_Schema.Tables 
	--WHERE  Table_Type = 'Base Table'
	--AND Table_Schema = 'public'
	
	UPDATE isee.set_test SET testid_ctl = NULL;
	UPDATE isee.set_test SET boarduuid = NULL;
	UPDATE isee.set_test SET current_test = NULL;
	UPDATE isee.set_test SET status = NULL;
	RETURN TRUE;
END;
$$;


ALTER FUNCTION isee.clear_set_test() OWNER TO admin;

--
-- Name: close_testbatch(uuid, integer); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.close_testbatch(bid uuid, tbid integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
    t record;
    result boolean;
    view_name varchar;
BEGIN
     -- Verify if the board and test exist in the control table
     SELECT INTO t * FROM isee.test_control WHERE boarduuid = bid AND testid_ctl = tbid;
     IF NOT FOUND THEN
        -- IF Test or board Not Found then Exit Now
        RAISE '#TEST not found# (%)', tbid;
     END IF;
     -- IF Found check the Test status and if the test is complete
     IF (t.status <> 'TERMINATED') AND (t.complete = TRUE) THEN
        -- IF Test is Not complete then it's not possible
        -- close this Test
        RAISE '#Test not complete# (%)', tbid;
     END IF;
     -- Verify if the Test is OK 
     result := isee.check_test_ok(tbid);
     IF result = FALSE THEN
     	-- Test Failed
        UPDATE isee.test_control SET status = 'TEST_FAILED' WHERE testid_ctl = tbid;
        UPDATE isee.test_control SET complete = 'FALSE' WHERE testid_ctl = tbid;
        UPDATE isee.set_test SET status = 'TEST_FAILED' WHERE testid_ctl = tbid;
        -- Destroy Table with Test cases left     
	view_name := 'test_left_' || tbid;
	EXECUTE 'DROP TABLE IF EXISTS ' || view_name;
	-- Test Fail
	RETURN FALSE;
        --RAISE '#TEST Failed# (%)', tbid;
     ELSE
	 UPDATE isee.test_control SET status = 'TEST_COMPLETE' WHERE testid_ctl = tbid;
         UPDATE isee.test_control SET complete = 'TRUE' WHERE testid_ctl = tbid;
         UPDATE isee.set_test SET status = 'TEST_COMPLETE' WHERE testid_ctl = tbid;
	 -- get variables from other tables
	 -- fill igep_ok board
	 IF EXISTS (SELECT FROM isee.igep_ok WHERE board_uuid = bid) THEN
		-- If a board already passed the test with OK then it will be found in igep_ok
		-- therefore we will do nothing because it is expected that nothing changed, also we avoid putting same board in two different manufacturer order of fabrication doing an erroneos UPDATE
		-- BOARD UUID EXIST, UPDATE ROW
		--UPDATE isee.igep_ok SET board_pid = (SELECT board_processor_id FROM isee.board WHERE board_uuid = bid) WHERE isee.igep_ok.board_uuid = bid;
		--UPDATE isee.igep_ok SET model = (SELECT modelid FROM (isee.board JOIN isee.board_model ON ((isee.board.mid = isee.board_model.mid))) WHERE isee.board.board_uuid = bid) WHERE isee.igep_ok.board_uuid = bid;
		--UPDATE isee.igep_ok SET variant = (SELECT variant FROM (isee.board JOIN isee.board_model ON ((isee.board.mid = isee.board_model.mid))) WHERE isee.board.board_uuid = bid) WHERE isee.igep_ok.board_uuid = bid;
		-- It is suposed that board manufacturer is set up when we deploy the entire DB in manufacturers host
		--UPDATE isee.igep_ok SET board_manufacturer = (SELECT board_manufacturer FROM isee.manufacturer) WHERE isee.igep_ok.board_uuid = bid;
		-- It is suposed that isee current order of fabrication is up to date thanks to nodejs setting it before we get here;
		--UPDATE isee.igep_ok SET order_of_fabrication = (SELECT current_order_of_fabrication FROM isee.test_defaults) WHERE isee.igep_ok.board_uuid = bid;
		--UPDATE isee.igep_ok SET manf_timestamp = (SELECT date_trunc('second',(SELECT board_creation FROM isee.board WHERE board_uuid = bid))::timestamp(0)) WHERE isee.igep_ok.board_uuid = bid;
		--UPDATE isee.igep_ok SET bmac0 = (SELECT bmac0 FROM isee.board WHERE board_uuid = bid) WHERE isee.igep_ok.board_uuid = bid;
		--UPDATE isee.igep_ok SET bmac1 = (SELECT ('02:00:00:00:00:00')::macaddr) WHERE isee.igep_ok.board_uuid = bid;
	 ELSE
		-- BOARD UUID DON'T EXIST, CREATE ROW
		--INSERT INTO isee.igep_ok (board_uuid, board_pid, model, variant, board_manufacturer, order_of_fabrication, manf_timestamp, bmac0, bmac1)
		INSERT INTO isee.igep_ok (board_uuid, board_pid, model, variant, order_of_fabrication, manf_timestamp, bmac0, bmac1)
		VALUES (
		bid, 
		(SELECT board_processor_id FROM isee.board WHERE board_uuid = bid), 
		(SELECT modelid FROM (isee.board JOIN isee.board_model ON ((isee.board.mid = isee.board_model.mid))) WHERE isee.board.board_uuid = bid), 
		(SELECT variant FROM (isee.board JOIN isee.board_model ON ((isee.board.mid = isee.board_model.mid))) WHERE isee.board.board_uuid = bid), 
		-- It is suposed that board manufacturer is set up when we deploy the entire DB in manufacturers host
		--(SELECT board_manufacturer FROM isee.manufacturer),
		-- It is suposed that isee current order of fabrication is up to date thanks to nodejs setting it before we get here; 
		(SELECT current_order_of_fabrication FROM isee.test_defaults), 
		(SELECT date_trunc('second',(SELECT board_creation FROM isee.board WHERE board_uuid = bid))::timestamp(0)), 
		(SELECT bmac0 FROM isee.board WHERE board_uuid = bid), (SELECT ('02:00:00:00:00:00')::macaddr));
		-- Increase counter of igep ok boards we need to avoid the bug of launching test without nodejs
		-- If we launch it this way, there is no new batch number created so we need to created it first
		IF EXISTS (SELECT FROM isee.manufacturer_pro WHERE order_of_fabrication = (SELECT current_order_of_fabrication FROM isee.test_defaults)) THEN
			UPDATE isee.manufacturer_pro SET actual_ok_count = isee.manufacturer_pro.actual_ok_count + 1 WHERE order_of_fabrication = (SELECT current_order_of_fabrication FROM isee.test_defaults);
		ELSE
			INSERT INTO isee.manufacturer_pro (order_of_fabrication, actual_ok_count)
			VALUES (
			(SELECT current_order_of_fabrication FROM isee.test_defaults),
			'1');
		END IF;
	 END IF;
     END IF;         
     -- Destroy Table with Test cases left     
     view_name := 'test_left_' || tbid;
     EXECUTE 'DROP TABLE IF EXISTS ' || view_name;
     -- Test OK
     RETURN TRUE;
END;
$$;


ALTER FUNCTION isee.close_testbatch(bid uuid, tbid integer) OWNER TO admin;

--
-- Name: create_board(character varying, character varying, character varying, macaddr); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.create_board(procid character varying, modid character varying, var character varying, bmac_ext macaddr) RETURNS uuid
    LANGUAGE plpgsql
    AS $$
DECLARE
	res uuid;
	mod_row record;
	macaux macaddr;
BEGIN
	res = NULL;
	
	-- Check if board exist
	IF EXISTS (SELECT FROM isee.board WHERE board_processor_id = procid) THEN
		-- Check if is marked as recyclable
		--IF (SELECT recyclable FROM isee.board WHERE board_processor_id = procid) = TRUE THEN
			-- if true, mac will be rehused also board_creation time shouldn't be rewritten, so only mark recyclable as FALSE again, board is fine now
			--UPDATE isee.board SET board_creation = now() WHERE board_processor_id = procid;
		--	UPDATE isee.board SET recyclable = False WHERE board_processor_id = procid;
		--ELSE
			-- if false do nothing, board already created and not recyclable, board is fine
		--END IF;
	-- If board exists we will return uuid in res variable
	SELECT board_uuid INTO res FROM isee.board WHERE board_processor_id = procid;
	ELSE
		-- Check if there is any rehusable board
		--IF EXISTS (SELECT FROM isee.board WHERE recyclable = True) THEN
			-- if rehusable board found, update all the fields but not macaddr because it should be rehused, also mark recyclable as FALSE, board is fine now
			-- Obtain macaddr to reupdate
		--	SELECT macaddr INTO macaux FROM isee.board WHERE recyclable = True ORDER BY board_creation limit 1;
			-- Obtain Model information from Models table
		--	SELECT INTO mod_row * FROM isee.board_model WHERE ( modelid = modid) AND ( variant = var);
		--	IF FOUND THEN
				-- Generate new uuid
		--		res = uuid_generate_v1();
				-- Update all fields
		--		UPDATE isee.board SET board_uuid = res WHERE macaddr = macaux;
		--		UPDATE isee.board SET board_processor_id = procid WHERE macaddr = macaux;
		--		UPDATE isee.board SET board_creation = now() WHERE macaddr = macaux;		
		--		UPDATE isee.board SET mid = mod_row.mid WHERE macaddr = macaux;
		--		UPDATE isee.board SET recyclable = False WHERE macaddr = macaux;
		--	ELSE
				-- No Valid Model Found  Raise Exception
		--		RAISE EXCEPTION '#Model not exist# (%,%,%)', modid, pcbid, assid;
		--	END IF;		
		--ELSE
		-- if no rehusable board found insert into the table with new macaddr based on count and mark recyclabel as FALSE, board is fine now
		-- Obtain Model information from Models table
		SELECT INTO mod_row * FROM isee.board_model WHERE ( modelid = modid) AND ( variant = var);
		-- Generate new uuid
		res = uuid_generate_v1();
		-- Check if macaddr is NULL (external macaddr can be provided
		IF bmac_ext is NULL THEN
			-- We generate the address
			-- ((default macaddr in bigint + count of total rows in bigint) this to hex(text) add a 0 to the left side) this casted to macaddr !!
			--OPTION 1 USE COUNT TO OBTAIN NEXT IN SEQUENCE, DO NOT REMOVE ROW OR IT WILL BUG !
			--macaux = (SELECT (SELECT CONCAT('0',(SELECT hex(((SELECT x'020000000000'::bigint) + (SELECT COUNT(*) FROM isee.board))))))::macaddr);
			-- OPTION 2 USE SEQUENCE TO OBTAIN NEXT IN SEQUENCE
			--nextval('isee.board_test_def_testid_seq'::regclass)
			--macaux = (SELECT (SELECT CONCAT('0',(SELECT hex(((SELECT x'020000000000'::bigint) + (SELECT nextval('isee.board_macaddr_seq'::regclass)))))))::macaddr);
			--SELECT ('x'|| (SELECT macaddr_text FROM isee.test_defaults))::bit(48)::bigint;
			macaux = (SELECT (SELECT CONCAT('0',(SELECT hex(((SELECT ('x'|| (SELECT starting_macaddr FROM isee.test_defaults))::bit(48)::bigint) + (SELECT nextval('isee.board_macaddr_seq'::regclass)))))))::macaddr);
		ELSE
			-- ELSE get the macaddr from bmac_ext
			macaux = bmac_ext; 
		END IF;
		-- INSERT THE BOARD with all the data
		BEGIN
			INSERT INTO isee.board (board_uuid, board_processor_id, bmac0, mid) VALUES ( res , procid, macaux, mod_row.mid);
		EXCEPTION WHEN unique_violation THEN
			RAISE '#TTTTT#  asfsfd';
		END;    
		--END IF; from rehusable IF clause
	END IF;
	-- Return uuid for this board
	RETURN res;
END;
$$;


ALTER FUNCTION isee.create_board(procid character varying, modid character varying, var character varying, bmac_ext macaddr) OWNER TO admin;

--
-- Name: create_model(character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.create_model(modid character varying, var character varying, descr character varying, tgid character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    a integer;
BEGIN     
     -- Check if the group has test defined
     --SELECT COUNT(*) INTO a FROM isee.board_test_def WHERE testgroupid = tgid;
     --IF a = 0 THEN
     --   RAISE '#Not test group defined# (%s)', tgid;
     --END IF;
    BEGIN 
		-- Create the new Model
    INSERT INTO isee.board_model (modelid, variant, des, testgroupid) VALUES (modid, var, descr, tgid) RETURNING mid INTO a;
    EXCEPTION WHEN unique_violation THEN
    	a := -1;
    END;
    RETURN a;
END;
$$;


ALTER FUNCTION isee.create_model(modid character varying, var character varying, descr character varying, tgid character varying) OWNER TO admin;

--
-- Name: create_temp_table_testbatch_left(uuid, integer); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.create_temp_table_testbatch_left(bid uuid, temp_table integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    view_name varchar;
    result integer;
BEGIN
     view_name := 'test_left_' || temp_table;
     -- Create Temp table
/*     EXECUTE 'CREATE TEMP TABLE ' || view_name || ' ON COMMIT PRESERVE ROWS AS '
              || ' SELECT testid FROM getTestlist(''' || bid
              || ''');'; */
     EXECUTE 'CREATE TABLE ' || view_name || ' AS '
              || ' SELECT testid FROM isee.gettestlist(''' || bid
              || ''');';                     
END;
$$;


ALTER FUNCTION isee.create_temp_table_testbatch_left(bid uuid, temp_table integer) OWNER TO admin;

--
-- Name: define_test(character varying, character varying, character varying); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.define_test(testname character varying, testdesc character varying, testattr character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE    
    a integer;
BEGIN
     BEGIN
          INSERT INTO isee.test_definition (testdefname, des, testfunc) VALUES (testname, testdesc, testattr) RETURNING testdefid INTO a;     
     EXCEPTION WHEN unique_violation THEN
            a := -1;
     END;     
     return a;
END;
$$;


ALTER FUNCTION isee.define_test(testname character varying, testdesc character varying, testattr character varying) OWNER TO admin;

--
-- Name: delete_batch(character varying); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.delete_batch(oof character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    IF EXISTS (SELECT FROM isee.manufacturer_pro WHERE order_of_fabrication = oof) THEN
	-- Delete from board table all igeps that corresponds to that batch number
	DELETE FROM isee.board WHERE board_uuid = (SELECT board_uuid FROM isee.igep_ok WHERE order_of_fabrication = oof);
	-- Delete from igep_ok table all igeps that corresponds to that batch bynber
	DELETE FROM isee.igep_ok WHERE order_of_fabrication = oof;
	-- Now we can delete that batch number from manufacturer_pro table
	DELETE FROM isee.manufacturer_pro WHERE order_of_fabrication = oof;
	RETURN TRUE;
    ELSE
	-- This batch number does not exist
	RETURN FALSE;
    END IF;

END;
$$;


ALTER FUNCTION isee.delete_batch(oof character varying) OWNER TO admin;

--
-- Name: delete_board(uuid); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.delete_board(bid uuid) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    IF EXISTS (SELECT FROM isee.board WHERE board_uuid = bid) THEN
    -- Delete igep board from board table
    DELETE FROM isee.board WHERE board_uuid = bid;
    -- Decrement batch number associated with that igep board
    UPDATE isee.manufacturer_pro SET actual_ok_count = isee.manufacturer_pro.actual_ok_count -1 WHERE order_of_fabrication = (SELECT order_of_fabrication FROM isee.igep_ok WHERE board_uuid = bid);
    -- Now we can delete that board from igep_ok table
    DELETE FROM isee.igep_ok WHERE board_uuid = bid;
    RETURN TRUE;
    ELSE
    -- This uuid number does not exist
    RETURN FALSE;
    END IF;

END;
$$;


ALTER FUNCTION isee.delete_board(bid uuid) OWNER TO admin;

--
-- Name: delete_db(); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.delete_db() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    DELETE FROM isee.board;
    DELETE FROM isee.board_test;
    DELETE FROM isee.manufacturer_pro;
    DELETE FROM isee.igep_ok;
    DELETE FROM isee.test_control;
    UPDATE isee.set_test SET testid_ctl = NULL;
    UPDATE isee.set_test SET boarduuid = NULL;
    UPDATE isee.set_test SET current_test = NULL;
    UPDATE isee.set_test SET status = NULL;
    -- Now we will reset sequences
    ALTER SEQUENCE isee.board_macaddr_seq RESTART;
    ALTER SEQUENCE isee.board_test_id_seq RESTART;
    ALTER SEQUENCE isee.test_control_testid_ctl_seq RESTART;
    -- Destroy temporal tables if any
    --SELECT 'DROP Table [' + Table_Schema + '].[' + Table_Name + ']' FROM   Information_Schema.Tables WHERE  Table_Type = 'Base Table' AND Table_Schema = 'public';
    RETURN TRUE;
END;
$$;


ALTER FUNCTION isee.delete_db() OWNER TO admin;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: board; Type: TABLE; Schema: isee; Owner: admin
--

CREATE TABLE isee.board (
    board_uuid uuid NOT NULL,
    board_processor_id character varying NOT NULL,
    board_creation timestamp without time zone DEFAULT now(),
    bmac0 macaddr,
    mid integer NOT NULL
);


ALTER TABLE isee.board OWNER TO admin;

--
-- Name: getboard_by_procid(character varying); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.getboard_by_procid(procid character varying) RETURNS SETOF isee.board
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
     RETURN QUERY 
     EXECUTE (
             format('SELECT * FROM isee.board WHERE board_processor_id = %s;', quote_literal(procid))
     );
END;
$$;


ALTER FUNCTION isee.getboard_by_procid(procid character varying) OWNER TO admin;

--
-- Name: getboard_eeprom(uuid); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.getboard_eeprom(bid uuid) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
result text;
BEGIN
result := (SELECT concat(board_uuid,board_pid,model,variant,order_of_fabrication,manf_timestamp,bmac0,bmac1) FROM isee.igep_ok WHERE board_uuid = bid);
RETURN result;
END;$$;


ALTER FUNCTION isee.getboard_eeprom(bid uuid) OWNER TO admin;

--
-- Name: getboard_uuid_by_procid(character varying); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.getboard_uuid_by_procid(procid character varying) RETURNS uuid
    LANGUAGE plpgsql
    AS $$
DECLARE
       t uuid;
BEGIN
     SELECT board_uuid INTO t FROM isee.board WHERE board_processor_id = procid;
     IF NOT FOUND THEN
        RETURN NULL;
     END IF;
     RETURN t;
END;
$$;


ALTER FUNCTION isee.getboard_uuid_by_procid(procid character varying) OWNER TO admin;

--
-- Name: gettestcompletelist(uuid); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.gettestcompletelist(bid uuid) RETURNS TABLE(testdefname character varying, des character varying, testfunc character varying, testparam character varying)
    LANGUAGE plpgsql
    AS $$
DECLARE
    TGroupID varchar;    
    --mid integer;
    --mod_row record;
BEGIN
    TGroupID = isee.gettestgroupid(bid);    
    RETURN QUERY 
    SELECT test_definition.testdefname, test_definition.des, test_definition.testfunc, board_test_def.testparam 
                  FROM (isee.board_test_def JOIN isee.test_definition ON 
                  (( isee.board_test_def.testdefid = isee.test_definition.testdefid))) 
                  WHERE isee.board_test_def.testgroupid =TGroupID;
END;
$$;


ALTER FUNCTION isee.gettestcompletelist(bid uuid) OWNER TO admin;

--
-- Name: gettestgroupid(uuid); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.gettestgroupid(bid uuid) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
    TGroupID varchar;
    mod_id integer;
	mod_row record;
BEGIN
     TGroupID = 'none';
     SELECT mid INTO mod_id FROM isee.board WHERE board_uuid = bid;
     IF FOUND THEN
        SELECT isee.board_model.testgroupid INTO TGroupID FROM isee.board_model WHERE isee.board_model.mid = mod_id;
        IF NOT FOUND THEN
           RAISE '#Board have not Test assigned# (%)', bid;
        END IF;         
     ELSE
         RAISE '#Board not found# (%)', bid;       
     END IF;
     RETURN TGroupID;
END;
$$;


ALTER FUNCTION isee.gettestgroupid(bid uuid) OWNER TO admin;

--
-- Name: gettestlist(uuid); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.gettestlist(bid uuid) RETURNS TABLE(testid integer, testgroupid character varying, testdefname character varying)
    LANGUAGE plpgsql
    AS $$
DECLARE
    TGroupID varchar;
    --mid integer;
    --mod_row record;
BEGIN
    TGroupID = isee.gettestgroupid(bid);
    RETURN QUERY 
           select board_test_def.testid, board_test_def.testgroupid, test_definition.testdefname
                  from (isee.board_test_def join isee.test_definition ON 
                  (( isee.board_test_def.testdefid = isee.test_definition.testdefid))) 
                  WHERE isee.board_test_def.testgroupid =TGroupID;
END;
$$;


ALTER FUNCTION isee.gettestlist(bid uuid) OWNER TO admin;

--
-- Name: gettestlistcomplete(uuid); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.gettestlistcomplete(bid uuid) RETURNS TABLE(testdefname character varying, des character varying, testfunc character varying, testparam character varying)
    LANGUAGE plpgsql
    AS $$
DECLARE
    TGroupID varchar;
    mid integer;
	mod_row record;
BEGIN
    TGroupID = isee.gettestgroupid(bid);
    RETURN QUERY 
           select board_test_def.testid, board_test_def.testgroupid, test_definition.testdefname
                  from (isee.board_test_def join isee.test_definition ON 
                  (( isee.board_test_def.testdefid = isee.test_definition.testdefid))) 
                  WHERE isee.board_test_def.testgroupid =TGroupID;
END;
$$;


ALTER FUNCTION isee.gettestlistcomplete(bid uuid) OWNER TO admin;

--
-- Name: open_testbatch(uuid); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.open_testbatch(bid uuid) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    result integer;
    a integer;
    view_name varchar;
BEGIN
     -- get Next ID
     SELECT MAX(testid_ctl) INTO a FROM isee.test_control;
     IF a IS NULL THEN        
        result := 1;       
     ELSE        
        result := a + 1;
     END IF;
     -- Create new Job
     INSERT INTO isee.test_control (boarduuid, testid_ctl) VALUES (bid, result);
     -- Create View
     -- PERFORM create_view_testbatch (result);
     PERFORM isee.create_temp_table_testbatch_left (bid, result);
     RETURN result;
END;
$$;


ALTER FUNCTION isee.open_testbatch(bid uuid) OWNER TO admin;

--
-- Name: test_cl(integer); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.test_cl(tid integer) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE    
	a varchar[];	
	t character varying;

BEGIN
	t := 'SELECT ARRAY (SELECT testdefname FROM isee.test_definition WHERE testdefid IN(SELECT testdefid FROM isee.board_test_def WHERE testid IN (SELECT testid FROM isee.board_test WHERE (testid_ctl = 238 AND result = FALSE))))';	EXECUTE t INTO a;
	EXECUTE t INTO a;
	UPDATE isee.set_test SET current_test = a WHERE station=1;      
	return a;
END;
$$;


ALTER FUNCTION isee.test_cl(tid integer) OWNER TO admin;

--
-- Name: test_func(uuid, integer, integer); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.test_func(bid uuid, exec_id integer, idid integer) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
    a integer;
    t character varying;
    t_id integer;
    pr varchar;
    
BEGIN
     -- Remove this test test from test list
     t := 'SELECT COUNT(*) FROM  test_left_' || idid;
	EXECUTE t INTO a;
     
     --SELECT COUNT(*) INTO pr FROM test_left_5;
     -- Check if we've more test to do
     --IF a = 0 THEN
     	-- Not have more test to do
    --    PERFORM isee.update_testbatch(bid, exec_id, 'TERMINATED');
     --ELSE
     	-- left more test
      --  PERFORM isee.update_testbatch(bid, exec_id, 'RUN');
     --END IF;
     RETURN a;              
END;
$$;


ALTER FUNCTION isee.test_func(bid uuid, exec_id integer, idid integer) OWNER TO admin;

--
-- Name: update_cpro(character varying); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.update_cpro(currentbn character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN	
	-- First we update current production number
	UPDATE isee.test_defaults SET current_order_of_fabrication = currentbn;
	-- Then we check if the the number is already registered as a batch number, if is do nothing, but if it doesn't then we need to insert it as this will be a new batch to produce
	IF EXISTS (SELECT FROM isee.manufacturer_pro WHERE order_of_fabrication = currentbn) THEN
	
	ELSE
               INSERT INTO isee.manufacturer_pro (order_of_fabrication, actual_ok_count) VALUES (currentbn , '0');
	END IF;
	RETURN TRUE;
END;$$;


ALTER FUNCTION isee.update_cpro(currentbn character varying) OWNER TO admin;

--
-- Name: update_set_test_row(integer, integer, uuid, character varying, character varying); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.update_set_test_row(nstation integer, tbid integer, bid uuid, ctest character varying, final_st character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
	IF final_st = 'FINISH' THEN
    	UPDATE isee.set_test SET testid_ctl = tbid, boarduuid = bid, current_test = ctest WHERE station = nstation;
	ELSE
	UPDATE isee.set_test SET testid_ctl = tbid, boarduuid = bid, current_test = ctest, status = final_st WHERE station = nstation;
	END IF;
END;
$$;


ALTER FUNCTION isee.update_set_test_row(nstation integer, tbid integer, bid uuid, ctest character varying, final_st character varying) OWNER TO admin;

--
-- Name: update_testbatch(uuid, integer, character varying); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.update_testbatch(bid uuid, tbid integer, new_st character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
     IF new_st = 'TERMINATED' THEN
         UPDATE isee.test_control SET status = new_st, complete = True WHERE boarduuid = bid AND testid_ctl = tbid;
     ELSE
         UPDATE isee.test_control SET status = new_st, complete = False WHERE boarduuid = bid AND testid_ctl = tbid;
     END IF;     
END;
$$;


ALTER FUNCTION isee.update_testbatch(bid uuid, tbid integer, new_st character varying) OWNER TO admin;

--
-- Name: uuid_macaddr(uuid); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.uuid_macaddr(id uuid) RETURNS macaddr
    LANGUAGE sql IMMUTABLE STRICT
    AS $$
  select substring(id::text, 25, 12)::macaddr
$$;


ALTER FUNCTION isee.uuid_macaddr(id uuid) OWNER TO admin;

--
-- Name: uuid_timestamp(uuid); Type: FUNCTION; Schema: isee; Owner: admin
--

CREATE FUNCTION isee.uuid_timestamp(id uuid) RETURNS timestamp with time zone
    LANGUAGE sql IMMUTABLE STRICT
    AS $$
  select TIMESTAMP WITH TIME ZONE 'epoch' +
      (((('x' || lpad(split_part(id::text, '-', 1), 16, '0'))::bit(64)::bigint) +
      (('x' || lpad(split_part(id::text, '-', 2), 16, '0'))::bit(64)::bigint << 32) +
      ((('x' || lpad(split_part(id::text, '-', 3), 16, '0'))::bit(64)::bigint&4095) << 48) - 122192928000000000) / 10000000 ) * INTERVAL '1 second';    
$$;


ALTER FUNCTION isee.uuid_timestamp(id uuid) OWNER TO admin;

--
-- Name: hex(bigint); Type: FUNCTION; Schema: public; Owner: admin
--

CREATE FUNCTION public.hex(bigint) RETURNS text
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
  select to_hex($1)
$_$;


ALTER FUNCTION public.hex(bigint) OWNER TO admin;

--
-- Name: board_macaddr_seq; Type: SEQUENCE; Schema: isee; Owner: admin
--

CREATE SEQUENCE isee.board_macaddr_seq
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 4294967295
    CACHE 1;


ALTER TABLE isee.board_macaddr_seq OWNER TO admin;

--
-- Name: board_model; Type: TABLE; Schema: isee; Owner: admin
--

CREATE TABLE isee.board_model (
    mid integer NOT NULL,
    modelid character varying NOT NULL,
    variant character varying NOT NULL,
    des character varying,
    testgroupid character varying NOT NULL
);


ALTER TABLE isee.board_model OWNER TO admin;

--
-- Name: board_model_mid_seq; Type: SEQUENCE; Schema: isee; Owner: admin
--

CREATE SEQUENCE isee.board_model_mid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE isee.board_model_mid_seq OWNER TO admin;

--
-- Name: board_model_mid_seq; Type: SEQUENCE OWNED BY; Schema: isee; Owner: admin
--

ALTER SEQUENCE isee.board_model_mid_seq OWNED BY isee.board_model.mid;


--
-- Name: board_test; Type: TABLE; Schema: isee; Owner: admin
--

CREATE TABLE isee.board_test (
    id integer NOT NULL,
    testid integer NOT NULL,
    board_uuid uuid NOT NULL,
    result boolean DEFAULT false,
    external_data character varying,
    ts timestamp without time zone DEFAULT now(),
    testid_ctl integer
);


ALTER TABLE isee.board_test OWNER TO admin;

--
-- Name: board_test_def; Type: TABLE; Schema: isee; Owner: admin
--

CREATE TABLE isee.board_test_def (
    testid integer NOT NULL,
    testgroupid character varying NOT NULL,
    testdefid integer NOT NULL,
    testparam character varying
);


ALTER TABLE isee.board_test_def OWNER TO admin;

--
-- Name: board_test_def_testid_seq; Type: SEQUENCE; Schema: isee; Owner: admin
--

CREATE SEQUENCE isee.board_test_def_testid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE isee.board_test_def_testid_seq OWNER TO admin;

--
-- Name: board_test_def_testid_seq; Type: SEQUENCE OWNED BY; Schema: isee; Owner: admin
--

ALTER SEQUENCE isee.board_test_def_testid_seq OWNED BY isee.board_test_def.testid;


--
-- Name: board_test_id_seq; Type: SEQUENCE; Schema: isee; Owner: admin
--

CREATE SEQUENCE isee.board_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE isee.board_test_id_seq OWNER TO admin;

--
-- Name: board_test_id_seq; Type: SEQUENCE OWNED BY; Schema: isee; Owner: admin
--

ALTER SEQUENCE isee.board_test_id_seq OWNED BY isee.board_test.id;


--
-- Name: igep_ok; Type: TABLE; Schema: isee; Owner: admin
--

CREATE TABLE isee.igep_ok (
    board_uuid uuid NOT NULL,
    board_pid character varying,
    model character varying,
    variant character varying,
    order_of_fabrication character varying,
    manf_timestamp timestamp without time zone,
    bmac0 macaddr,
    bmac1 macaddr
);


ALTER TABLE isee.igep_ok OWNER TO admin;

--
-- Name: manufacturer_pro; Type: TABLE; Schema: isee; Owner: admin
--

CREATE TABLE isee.manufacturer_pro (
    order_of_fabrication character varying NOT NULL,
    actual_ok_count integer
);


ALTER TABLE isee.manufacturer_pro OWNER TO admin;

--
-- Name: set_test; Type: TABLE; Schema: isee; Owner: admin
--

CREATE TABLE isee.set_test (
    station integer NOT NULL,
    testid_ctl integer,
    boarduuid uuid,
    current_test character varying,
    status character varying
);


ALTER TABLE isee.set_test OWNER TO admin;

--
-- Name: test_control; Type: TABLE; Schema: isee; Owner: admin
--

CREATE TABLE isee.test_control (
    testid_ctl integer NOT NULL,
    boarduuid uuid NOT NULL,
    complete boolean DEFAULT false,
    status character varying(30)
);


ALTER TABLE isee.test_control OWNER TO admin;

--
-- Name: test_control_testid_ctl_seq; Type: SEQUENCE; Schema: isee; Owner: admin
--

CREATE SEQUENCE isee.test_control_testid_ctl_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE isee.test_control_testid_ctl_seq OWNER TO admin;

--
-- Name: test_control_testid_ctl_seq; Type: SEQUENCE OWNED BY; Schema: isee; Owner: admin
--

ALTER SEQUENCE isee.test_control_testid_ctl_seq OWNED BY isee.test_control.testid_ctl;


--
-- Name: test_defaults; Type: TABLE; Schema: isee; Owner: admin
--

CREATE TABLE isee.test_defaults (
    board_manufacturer character varying,
    starting_macaddr text,
    current_order_of_fabrication character varying NOT NULL
);


ALTER TABLE isee.test_defaults OWNER TO admin;

--
-- Name: test_definition; Type: TABLE; Schema: isee; Owner: admin
--

CREATE TABLE isee.test_definition (
    testdefid integer NOT NULL,
    testdefname character varying NOT NULL,
    des character varying(30) NOT NULL,
    testfunc character varying
);


ALTER TABLE isee.test_definition OWNER TO admin;

--
-- Name: test_definition_testdefid_seq; Type: SEQUENCE; Schema: isee; Owner: admin
--

CREATE SEQUENCE isee.test_definition_testdefid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE isee.test_definition_testdefid_seq OWNER TO admin;

--
-- Name: test_definition_testdefid_seq; Type: SEQUENCE OWNED BY; Schema: isee; Owner: admin
--

ALTER SEQUENCE isee.test_definition_testdefid_seq OWNED BY isee.test_definition.testdefid;


--
-- Name: mid; Type: DEFAULT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.board_model ALTER COLUMN mid SET DEFAULT nextval('isee.board_model_mid_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.board_test ALTER COLUMN id SET DEFAULT nextval('isee.board_test_id_seq'::regclass);


--
-- Name: testid; Type: DEFAULT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.board_test_def ALTER COLUMN testid SET DEFAULT nextval('isee.board_test_def_testid_seq'::regclass);


--
-- Name: testid_ctl; Type: DEFAULT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.test_control ALTER COLUMN testid_ctl SET DEFAULT nextval('isee.test_control_testid_ctl_seq'::regclass);


--
-- Name: testdefid; Type: DEFAULT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.test_definition ALTER COLUMN testdefid SET DEFAULT nextval('isee.test_definition_testdefid_seq'::regclass);


--
-- Data for Name: board; Type: TABLE DATA; Schema: isee; Owner: admin
--

COPY isee.board (board_uuid, board_processor_id, board_creation, bmac0, mid) FROM stdin;
\.


--
-- Name: board_macaddr_seq; Type: SEQUENCE SET; Schema: isee; Owner: admin
--

SELECT pg_catalog.setval('isee.board_macaddr_seq', 1, false);


--
-- Data for Name: board_model; Type: TABLE DATA; Schema: isee; Owner: admin
--

COPY isee.board_model (mid, modelid, variant, des, testgroupid) FROM stdin;
1	IGEP0046	DWU1-4XXX	IGEP SMARC MODULE iMX6 DUALLITE WIFI DDR 1GB FLASH 4GB	IGEP0046-DWU1-4XXX
2	IGEP0046	DWQ2-8SXX	IGEP SMARC MODULE iMX6 QUAD WIFI DDR 2GB FLASH 8GB	IGEP0046-DWQ2-8SXX
3	IGEP0046	DWU2-4XXX	IGEP SMARC MODULE iMX6 DUALLITE WIFI DDR 2GB FLASH 4GB	IGEP0046-DWU2-4XXX
4	IGEP0046	DWQ1-8SXX	IGEP SMARC MODULE iMX6 QUAD WIFI DDR 1GB FLASH 8GB	IGEP0046-DWQ1-8SXX
5	IGEP0046	DWE2-8SXX	IGEP SMARC MODULE iMX6 DUALPLUS WIFI DDR 2GB FLASH 8GB	IGEP0046-DWE2-8SXX
6	IGEP0046	DWR2-8SXX	IGEP SMARC MODULE iMX6 QUADPLUS WIFI DDR 2GB FLASH 8GB	IGEP0046-DWR2-8SXX
7	IGEP0046	DWSH-4XXX	IGEP SMARC MODULE iMX6 SOLO WIFI DDR 512MB FLASH 4GB	IGEP0046-DWSH-4XXX
8	IGEP0046	DWD2-8SXX	IGEP SMARC MODULE iMX6 DUAL WIFI DDR 2GB FLASH 8GB	IGEP0046-DWD2-8SXX
9	IGEP0046	DXU2-4XXX	IGEP SMARC MODULE iMX6 DUALLITE NO WIFI DDR 2GB FLASH 4GB	IGEP0046-DXU2-4XXX
10	IGEP0034	AW2Q-OEXX	IGEP SMARC MODULE AM3352 WIFI DDR 256MB FLASH 128MB	IGEP0034-AW2Q-OEXX
11	IGEP0034	AW4H-HEGV	IGEP SMARC MODULE AM3354 WIFI DDR 512MB FLASH 512MB	IGEP0034-AW4H-HEGV
12	IGEP0034	AX2H-HXXX	IGEP SMARC MODULE AM3352 WIFI DDR 512MB FLASH 512MB	IGEP0034-AX2H-HXXX
13	IGEP0034	AX2H-HC36	IGEP SMARC MODULE AM3352 WIFI ETHERNET DDR 512MB FLASH 512MB	IGEP0034-AX2H-HC36
14	SOPA0000	BW2Q-QXXX	SOPA0000 BOARD AM335X RB WIFI ETHERNET DDR 256MB FLASH 256MB	SOPA0000-BW2Q-QXXX
15	SOPA0000	BX2Q-QXXX	SOPA0000 BOARD AM335X RB NO WIFI ETHERNET DDR 256MB FLASH 256MB	SOPA0000-BX2Q-QXXX
16	IGEP0000	TEST-TEST	IGEP BOARD TO TEST FUNCTIONALITY OF THE TEST	IGEP0000-TEST-TEST
\.


--
-- Name: board_model_mid_seq; Type: SEQUENCE SET; Schema: isee; Owner: admin
--

SELECT pg_catalog.setval('isee.board_model_mid_seq', 16, true);


--
-- Data for Name: board_test; Type: TABLE DATA; Schema: isee; Owner: admin
--

COPY isee.board_test (id, testid, board_uuid, result, external_data, ts, testid_ctl) FROM stdin;
\.


--
-- Data for Name: board_test_def; Type: TABLE DATA; Schema: isee; Owner: admin
--

COPY isee.board_test_def (testid, testgroupid, testdefid, testparam) FROM stdin;
1	IGEP0046-DWE2-8SXX	2	cat /proc/cpuinfo
2	IGEP0046-DWE2-8SXX	1	df -ah
3	IGEP0046-DWE2-8SXX	3	192.168.2.171;400
4	IGEP0046-DWE2-8SXX	8	2000
5	IGEP0046-DWE2-8SXX	9	/home/carlosa/Music/dtmf-13579.wav
6	IGEP0046-DWE2-8SXX	10	TEST;2
7	IGEP0046-DWE2-8SXX	14	0;0x3c
8	IGEP0046-DWE2-8SXX	15	1;0x08/0x50
9	IGEP0046-DWE2-8SXX	16	2;0x1b/0x50
10	IGEP0046-DWE2-8SXX	18	
11	IGEP0046-DWE2-8SXX	19	/dev/ttymxc3;115200
12	IGEP0046-DWE2-8SXX	19	/dev/ttymxc0;115200
13	IGEP0046-DWE2-8SXX	20	TESTOTG; 1
14	IGEP0046-DWE2-8SXX	21	fb0
36	SOPA0000-BX2Q-QXXX	2	cat /proc/cpuinfo
37	SOPA0000-BX2Q-QXXX	1	dmesg
38	SOPA0000-BX2Q-QXXX	3	192.168.2.171;70
39	SOPA0000-BX2Q-QXXX	8	240
40	SOPA0000-BX2Q-QXXX	15	1;0x2d/0x50
41	SOPA0000-BX2Q-QXXX	16	2;0x6f
42	SOPA0000-BX2Q-QXXX	18	
43	SOPA0000-BX2Q-QXXX	22	SOPA
44	SOPA0000-BX2Q-QXXX	24	/dev/rtc0
24	SOPA0000-BW2Q-QXXX	2	cat /proc/cpuinfo
25	SOPA0000-BW2Q-QXXX	1	df -ah
45	SOPA0000-BX2Q-QXXX	25	/dev/ttyO4;/dev/ttyO3;115200
27	SOPA0000-BW2Q-QXXX	8	240
46	SOPA0000-BX2Q-QXXX	25	/dev/ttyO4;/dev/ttyO5;115200
29	SOPA0000-BW2Q-QXXX	16	2;0x6f
30	SOPA0000-BW2Q-QXXX	18	
28	SOPA0000-BW2Q-QXXX	15	1;0x2d
31	SOPA0000-BW2Q-QXXX	22	SOPA
32	SOPA0000-BW2Q-QXXX	23	100
33	SOPA0000-BW2Q-QXXX	24	/dev/rtc0
34	SOPA0000-BW2Q-QXXX	25	/dev/ttyO4;/dev/ttyO3;115200
35	SOPA0000-BW2Q-QXXX	25	/dev/ttyO4;/dev/ttyO5;115200
26	SOPA0000-BW2Q-QXXX	3	192.168.2.171;69
49	IGEP0046-DWU2-4XXX	2	cat /proc/cpuinfo
50	IGEP0046-DWU2-4XXX	1	dmesg
51	IGEP0046-DWU2-4XXX	3	192.168.2.171;90
52	IGEP0046-DWU2-4XXX	8	2000
53	IGEP0046-DWU2-4XXX	9	./test/files/dtmf-13579.wav
55	IGEP0046-DWU2-4XXX	20	TESTOTG;1
57	IGEP0046-DWU2-4XXX	15	1;0x08/0x50
58	IGEP0046-DWU2-4XXX	16	2;0x1b/0x50
59	IGEP0046-DWU2-4XXX	19	/dev/ttymxc3;115200
60	IGEP0046-DWU2-4XXX	19	/dev/ttymxc1;115200
61	IGEP0046-DWU2-4XXX	18	
63	IGEP0046-DWU2-4XXX	21	fb0
54	IGEP0046-DWU2-4XXX	10	TEST;1
64	IGEP0046-DWU2-4XXX	26	0.3;1.2
62	IGEP0046-DWU2-4XXX	23	95
48	IGEP0046-DWE2-8SXX	26	0.3;1
67	IGEP0046-DWU2-4XXX	27	
47	SOPA0000-BW2Q-QXXX	26	0.4;1 
69	SOPA0000-BW2Q-QXXX	19	/dev/ttyO0;115200
71	SOPA0000-BW2Q-QXXX	27	
72	IGEP0046-DWQ1-8SXX	2	cat /proc/cpuinfo
73	IGEP0046-DWQ1-8SXX	1	dmesg
74	IGEP0046-DWQ1-8SXX	2	lsblk -a
75	IGEP0046-DWQ1-8SXX	1	lsusb
76	IGEP0046-DWQ1-8SXX	1	lsmod
80	IGEP0046-DWQ1-8SXX	15	1;0x08/0x50
81	IGEP0046-DWQ1-8SXX	16	2;0x1b/0x50
82	IGEP0046-DWQ1-8SXX	18	
84	IGEP0046-DWQ1-8SXX	19	/dev/ttymxc3;115200
86	IGEP0046-DWQ1-8SXX	20	TESTOTG;1
87	IGEP0046-DWQ1-8SXX	21	fb0
88	IGEP0046-DWQ1-8SXX	27	
83	IGEP0046-DWQ1-8SXX	23	95
85	IGEP0046-DWQ1-8SXX	19	/dev/ttymxc1;115200
77	IGEP0046-DWQ1-8SXX	3	192.168.2.171;90
78	IGEP0046-DWQ1-8SXX	8	1000
79	IGEP0046-DWQ1-8SXX	26	0.3;1.2
89	IGEP0046-DWQ1-8SXX	28	TESTSATA;1
90	IGEP0046-DWQ1-8SXX	9	./test/files/dtmf-13579.wav
91	IGEP0046-DXU2-4XXX	1	dmesg
92	IGEP0046-DXU2-4XXX	2	cat /proc/cpuinfo
93	IGEP0046-DXU2-4XXX	32	ifconfig -a
94	IGEP0046-DXU2-4XXX	31	lsmod
95	IGEP0046-DXU2-4XXX	29	lsblk -a
96	IGEP0046-DXU2-4XXX	30	lsusb -v
97	IGEP0046-DXU2-4XXX	8	2000
98	IGEP0046-DXU2-4XXX	3	192.168.2.171;90
99	IGEP0046-DXU2-4XXX	9	./test/files/dtmf-13579.wav
100	IGEP0046-DXU2-4XXX	15	1;0x08/0x50
101	IGEP0046-DXU2-4XXX	16	2;0x1b/0x50
102	IGEP0046-DXU2-4XXX	26	0.3;1.2
103	IGEP0046-DXU2-4XXX	18	
104	IGEP0046-DXU2-4XXX	19	/dev/ttymxc1;115200
105	IGEP0046-DXU2-4XXX	19	/dev/ttymxc0;115200
106	IGEP0046-DXU2-4XXX	19	/dev/ttymxc3;115200
107	IGEP0046-DXU2-4XXX	10	TEST;1
108	IGEP0046-DXU2-4XXX	20	TESTOTG;1
109	IGEP0046-DXU2-4XXX	21	fb0
110	IGEP0046-DXU2-4XXX	27	
111	IGEP0000-TEST-TEST	1	dmesg
113	IGEP0000-TEST-TEST	8	1000
114	IGEP0000-TEST-TEST	15	1;0x08/0x50
115	IGEP0000-TEST-TEST	16	2;0x1b/0x50
116	IGEP0000-TEST-TEST	18	
117	IGEP0000-TEST-TEST	27	
112	IGEP0000-TEST-TEST	3	192.168.171;90
\.


--
-- Name: board_test_def_testid_seq; Type: SEQUENCE SET; Schema: isee; Owner: admin
--

SELECT pg_catalog.setval('isee.board_test_def_testid_seq', 117, true);


--
-- Name: board_test_id_seq; Type: SEQUENCE SET; Schema: isee; Owner: admin
--

SELECT pg_catalog.setval('isee.board_test_id_seq', 1, false);


--
-- Data for Name: igep_ok; Type: TABLE DATA; Schema: isee; Owner: admin
--

COPY isee.igep_ok (board_uuid, board_pid, model, variant, order_of_fabrication, manf_timestamp, bmac0, bmac1) FROM stdin;
\.


--
-- Data for Name: manufacturer_pro; Type: TABLE DATA; Schema: isee; Owner: admin
--

COPY isee.manufacturer_pro (order_of_fabrication, actual_ok_count) FROM stdin;
\.


--
-- Data for Name: set_test; Type: TABLE DATA; Schema: isee; Owner: admin
--

COPY isee.set_test (station, testid_ctl, boarduuid, current_test, status) FROM stdin;
3	\N	\N	\N	\N
2	\N	\N	\N	\N
1	\N	\N	\N	\N
\.


--
-- Data for Name: test_control; Type: TABLE DATA; Schema: isee; Owner: admin
--

COPY isee.test_control (testid_ctl, boarduuid, complete, status) FROM stdin;
\.


--
-- Name: test_control_testid_ctl_seq; Type: SEQUENCE SET; Schema: isee; Owner: admin
--

SELECT pg_catalog.setval('isee.test_control_testid_ctl_seq', 1, false);


--
-- Data for Name: test_defaults; Type: TABLE DATA; Schema: isee; Owner: admin
--

COPY isee.test_defaults (board_manufacturer, starting_macaddr, current_order_of_fabrication) FROM stdin;
KOMITECH	020400000000	777777
\.


--
-- Data for Name: test_definition; Type: TABLE DATA; Schema: isee; Owner: admin
--

COPY isee.test_definition (testdefid, testdefname, des, testfunc) FROM stdin;
1	DMESG	Get dmesg output	TestSysCommand
2	CPUINFO	Get cpu information	TestSysCommand
3	ETHERNET	Iperf eth test	Qethernet
8	RAMSIZE	Ram total size check	Qram
9	AUDIO	Audio loopback test	Qaudio
10	USBHOST	USB HOST test	Qusb
14	I2C-0	Test slaves on i2c-0	Qi2c
15	I2C-1	Test slaves on i2c-1	Qi2c
16	I2C-2	Test slaves on i2c-2	Qi2c
17	I2C-3	Test slaves on i2c-3	Qi2c
18	EEPROM	Write and read on the EEPROM	Qeeprom
19	SERIAL	Serial loopback write & read	Qserial
20	USBOTG	USB OTG pendrive based test	Qusb
21	HDMI	Test pattern shown is correct	Qscreen
22	BUTTON	Wait for button be pressed	Qbutton
23	WIFI	Check WIFI connection/signal	Qwifi
24	RTC	Check RTC working	Qrtc
25	SERIALDUPLEX	Communicate 2 serial ports	Qduplex
26	CONSUMPTION	Check current consumption	Qamp
27	FLASHER	Flash u-boot inside board	Qflasher
28	SATA	Test SATA disk	Qusb
29	LSBLK	Test lsblk -a output	TestSysCommand
30	LSUSB	Test lsusb output	TestSysCommand
31	LSMOD	Test lsmod output	TestSysCommand
32	IFCONFIG	Test ifconfig -a output	TestSysCommand
\.


--
-- Name: test_definition_testdefid_seq; Type: SEQUENCE SET; Schema: isee; Owner: admin
--

SELECT pg_catalog.setval('isee.test_definition_testdefid_seq', 32, true);


--
-- Name: board_model_pkey; Type: CONSTRAINT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.board_model
    ADD CONSTRAINT board_model_pkey PRIMARY KEY (mid);


--
-- Name: board_model_testgroupid_key; Type: CONSTRAINT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.board_model
    ADD CONSTRAINT board_model_testgroupid_key UNIQUE (testgroupid);


--
-- Name: board_pkey; Type: CONSTRAINT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.board
    ADD CONSTRAINT board_pkey PRIMARY KEY (board_uuid, board_processor_id);


--
-- Name: board_test_def_pkey; Type: CONSTRAINT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.board_test_def
    ADD CONSTRAINT board_test_def_pkey PRIMARY KEY (testid);


--
-- Name: board_test_def_testgroupid_testdefid_key; Type: CONSTRAINT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.board_test_def
    ADD CONSTRAINT board_test_def_testgroupid_testdefid_key UNIQUE (testgroupid, testdefid, testparam);


--
-- Name: board_test_pkey; Type: CONSTRAINT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.board_test
    ADD CONSTRAINT board_test_pkey PRIMARY KEY (id);


--
-- Name: igep_ok_pkey; Type: CONSTRAINT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.igep_ok
    ADD CONSTRAINT igep_ok_pkey PRIMARY KEY (board_uuid);


--
-- Name: manufacturer_pro_pkey; Type: CONSTRAINT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.manufacturer_pro
    ADD CONSTRAINT manufacturer_pro_pkey PRIMARY KEY (order_of_fabrication);


--
-- Name: set_test_pkey; Type: CONSTRAINT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.set_test
    ADD CONSTRAINT set_test_pkey PRIMARY KEY (station);


--
-- Name: test_control_pkey; Type: CONSTRAINT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.test_control
    ADD CONSTRAINT test_control_pkey PRIMARY KEY (testid_ctl);


--
-- Name: test_defaults_pkey; Type: CONSTRAINT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.test_defaults
    ADD CONSTRAINT test_defaults_pkey PRIMARY KEY (current_order_of_fabrication);


--
-- Name: test_definition_pkey; Type: CONSTRAINT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.test_definition
    ADD CONSTRAINT test_definition_pkey PRIMARY KEY (testdefid);


--
-- Name: test_definition_testdefname_key; Type: CONSTRAINT; Schema: isee; Owner: admin
--

ALTER TABLE ONLY isee.test_definition
    ADD CONSTRAINT test_definition_testdefname_key UNIQUE (testdefname);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

