-- Sequence: isee.test_definition_testdefid_seq

-- DROP SEQUENCE isee.test_definition_testdefid_seq;

CREATE SEQUENCE isee.test_definition_testdefid_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 32
  CACHE 1;
ALTER TABLE isee.test_definition_testdefid_seq
  OWNER TO admin;
