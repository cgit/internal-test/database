-- Sequence: isee.board_macaddr_seq

-- DROP SEQUENCE isee.board_macaddr_seq;

CREATE SEQUENCE isee.board_macaddr_seq
  INCREMENT 1
  MINVALUE 0
  MAXVALUE 4294967295
  START 1
  CACHE 1;
ALTER TABLE isee.board_macaddr_seq
  OWNER TO admin;
