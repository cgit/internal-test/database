-- Sequence: isee.test_control_testid_ctl_seq

-- DROP SEQUENCE isee.test_control_testid_ctl_seq;

CREATE SEQUENCE isee.test_control_testid_ctl_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE isee.test_control_testid_ctl_seq
  OWNER TO admin;
