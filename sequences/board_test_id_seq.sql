-- Sequence: isee.board_test_id_seq

-- DROP SEQUENCE isee.board_test_id_seq;

CREATE SEQUENCE isee.board_test_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE isee.board_test_id_seq
  OWNER TO admin;
