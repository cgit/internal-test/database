-- Sequence: isee.board_model_mid_seq

-- DROP SEQUENCE isee.board_model_mid_seq;

CREATE SEQUENCE isee.board_model_mid_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 16
  CACHE 1;
ALTER TABLE isee.board_model_mid_seq
  OWNER TO admin;
