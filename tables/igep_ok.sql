-- Table: isee.igep_ok

-- DROP TABLE isee.igep_ok;

CREATE TABLE isee.igep_ok
(
  board_uuid uuid NOT NULL,
  board_pid character varying,
  model character varying,
  variant character varying,
  order_of_fabrication character varying,
  manf_timestamp timestamp without time zone,
  bmac0 macaddr,
  bmac1 macaddr,
  CONSTRAINT igep_ok_pkey PRIMARY KEY (board_uuid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE isee.igep_ok
  OWNER TO admin;
