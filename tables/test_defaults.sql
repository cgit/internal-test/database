-- Table: isee.test_defaults

-- DROP TABLE isee.test_defaults;

CREATE TABLE isee.test_defaults
(
  board_manufacturer character varying,
  starting_macaddr text,
  current_order_of_fabrication character varying NOT NULL,
  CONSTRAINT test_defaults_pkey PRIMARY KEY (current_order_of_fabrication)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE isee.test_defaults
  OWNER TO admin;
