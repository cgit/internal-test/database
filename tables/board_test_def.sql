-- Table: isee.board_test_def

-- DROP TABLE isee.board_test_def;

CREATE TABLE isee.board_test_def
(
  testid serial NOT NULL,
  testgroupid character varying NOT NULL,
  testdefid integer NOT NULL,
  testparam character varying,
  CONSTRAINT board_test_def_pkey PRIMARY KEY (testid),
  CONSTRAINT board_test_def_testgroupid_testdefid_key UNIQUE (testgroupid, testdefid, testparam)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE isee.board_test_def
  OWNER TO admin;
