-- Table: isee.board_test

-- DROP TABLE isee.board_test;

CREATE TABLE isee.board_test
(
  id serial NOT NULL,
  testid integer NOT NULL,
  board_uuid uuid NOT NULL,
  result boolean DEFAULT false,
  external_data character varying,
  ts timestamp without time zone DEFAULT now(),
  testid_ctl integer,
  CONSTRAINT board_test_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE isee.board_test
  OWNER TO admin;
