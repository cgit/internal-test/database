-- Table: isee.test_control

-- DROP TABLE isee.test_control;

CREATE TABLE isee.test_control
(
  testid_ctl serial NOT NULL,
  boarduuid uuid NOT NULL,
  complete boolean DEFAULT false,
  status character varying(30),
  CONSTRAINT test_control_pkey PRIMARY KEY (testid_ctl)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE isee.test_control
  OWNER TO admin;
