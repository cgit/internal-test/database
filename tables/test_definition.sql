-- Table: isee.test_definition

-- DROP TABLE isee.test_definition;

CREATE TABLE isee.test_definition
(
  testdefid serial NOT NULL,
  testdefname character varying NOT NULL,
  des character varying(30) NOT NULL,
  testfunc character varying,
  CONSTRAINT test_definition_pkey PRIMARY KEY (testdefid),
  CONSTRAINT test_definition_testdefname_key UNIQUE (testdefname)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE isee.test_definition
  OWNER TO admin;
