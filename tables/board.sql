-- Table: isee.board

-- DROP TABLE isee.board;

CREATE TABLE isee.board
(
  board_uuid uuid NOT NULL,
  board_processor_id character varying NOT NULL,
  board_creation timestamp without time zone DEFAULT now(),
  bmac0 macaddr,
  mid integer NOT NULL,
  CONSTRAINT board_pkey PRIMARY KEY (board_uuid, board_processor_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE isee.board
  OWNER TO admin;
