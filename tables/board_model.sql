-- Table: isee.board_model

-- DROP TABLE isee.board_model;

CREATE TABLE isee.board_model
(
  mid serial NOT NULL,
  modelid character varying NOT NULL,
  variant character varying NOT NULL,
  des character varying,
  testgroupid character varying NOT NULL,
  CONSTRAINT board_model_pkey PRIMARY KEY (mid),
  CONSTRAINT board_model_testgroupid_key UNIQUE (testgroupid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE isee.board_model
  OWNER TO admin;
