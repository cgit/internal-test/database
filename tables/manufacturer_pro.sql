-- Table: isee.manufacturer_pro

-- DROP TABLE isee.manufacturer_pro;

CREATE TABLE isee.manufacturer_pro
(
  order_of_fabrication character varying NOT NULL,
  actual_ok_count integer,
  CONSTRAINT manufacturer_pro_pkey PRIMARY KEY (order_of_fabrication)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE isee.manufacturer_pro
  OWNER TO admin;
