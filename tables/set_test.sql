-- Table: isee.set_test

-- DROP TABLE isee.set_test;

CREATE TABLE isee.set_test
(
  station integer NOT NULL,
  testid_ctl integer,
  boarduuid uuid,
  current_test character varying,
  status character varying,
  CONSTRAINT set_test_pkey PRIMARY KEY (station)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE isee.set_test
  OWNER TO admin;
