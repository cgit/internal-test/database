﻿-- Function: isee.getboard_uuid_by_procid(character varying)

-- DROP FUNCTION isee.getboard_uuid_by_procid(character varying);

CREATE OR REPLACE FUNCTION isee.getboard_uuid_by_procid(procid character varying)
  RETURNS uuid AS
$BODY$
DECLARE
       t uuid;
BEGIN
     SELECT board_uuid INTO t FROM isee.board WHERE board_processor_id = procid;
     IF NOT FOUND THEN
        RETURN NULL;
     END IF;
     RETURN t;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.getboard_uuid_by_procid(character varying)
  OWNER TO admin;
