-- Function: isee.getboard_eeprom(uuid)

-- DROP FUNCTION isee.getboard_eeprom(uuid);

CREATE OR REPLACE FUNCTION isee.getboard_eeprom(bid uuid)
  RETURNS text AS
$BODY$
DECLARE
result text;
BEGIN
result := (SELECT concat(board_uuid,board_pid,model,variant,order_of_fabrication,manf_timestamp,bmac0,bmac1) FROM isee.igep_ok WHERE board_uuid = bid);
RETURN result;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.getboard_eeprom(uuid)
  OWNER TO admin;
