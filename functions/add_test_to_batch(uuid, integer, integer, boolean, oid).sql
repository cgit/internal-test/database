﻿-- Function: isee.add_test_to_batch(uuid, integer, integer, boolean, oid)

-- DROP FUNCTION isee.add_test_to_batch(uuid, integer, integer, boolean, oid);

CREATE OR REPLACE FUNCTION isee.add_test_to_batch(
    bid uuid,
    testid integer,
    exec_id integer,
    res boolean,
    dat oid)
  RETURNS boolean AS
$BODY$
DECLARE
    a integer;
    t varchar;
BEGIN
     -- Remove this test test from test list
     t := 'test_left_' || exec_id;
     EXECUTE 'DELETE FROM ' || t || ' WHERE testid = ' || testid;
     -- INSERT Test Result
     INSERT INTO board_test (testid, board_uuid, result, external_data, testid_ctl) 
            VALUES (testid, bid, res, dat, exec_id);
     -- Count number of test remain in the list
     SELECT COUNT(*) INTO a FROM t;
     -- Check if we've more test to do
     IF a = 0 THEN
        -- Not have more test to do
        PERFORM isee.update_testbatch(bid, exec_id, "TERMINATED");
     ELSE
        -- left more test
        PERFORM isee.update_testbatch(bid, exec_id, "RUN");
     END IF;
     RETURN true;              
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.add_test_to_batch(uuid, integer, integer, boolean, oid)
  OWNER TO admin;
