﻿-- Function: isee.clear_set_test()

-- DROP FUNCTION isee.clear_set_test();

CREATE OR REPLACE FUNCTION isee.clear_set_test()
  RETURNS boolean AS
$BODY$
DECLARE
BEGIN
	-- Destroy Table with Test cases left     
	--SELECT 'DROP Table [' + Table_Schema + '].[' + Table_Name + ']' 
	--FROM   Information_Schema.Tables 
	--WHERE  Table_Type = 'Base Table'
	--AND Table_Schema = 'public'
	
	UPDATE isee.set_test SET testid_ctl = NULL;
	UPDATE isee.set_test SET boarduuid = NULL;
	UPDATE isee.set_test SET current_test = NULL;
	UPDATE isee.set_test SET status = NULL;
	RETURN TRUE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.clear_set_test()
  OWNER TO admin;
