-- Function: isee.test_cl(integer)

-- DROP FUNCTION isee.test_cl(integer);

CREATE OR REPLACE FUNCTION isee.test_cl(tid integer)
  RETURNS character varying AS
$BODY$
DECLARE    
	a varchar[];	
	t character varying;

BEGIN
	t := 'SELECT ARRAY (SELECT testdefname FROM isee.test_definition WHERE testdefid IN(SELECT testdefid FROM isee.board_test_def WHERE testid IN (SELECT testid FROM isee.board_test WHERE (testid_ctl = 238 AND result = FALSE))))';	EXECUTE t INTO a;
	EXECUTE t INTO a;
	UPDATE isee.set_test SET current_test = a WHERE station=1;      
	return a;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.test_cl(integer)
  OWNER TO admin;
