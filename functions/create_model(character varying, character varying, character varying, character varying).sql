﻿-- Function: isee.create_model(character varying, character varying, character varying, character varying)

-- DROP FUNCTION isee.create_model(character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION isee.create_model(
    modid character varying,
    var character varying,
    descr character varying,
    tgid character varying)
  RETURNS integer AS
$BODY$
DECLARE
    a integer;
BEGIN     
     -- Check if the group has test defined
     --SELECT COUNT(*) INTO a FROM isee.board_test_def WHERE testgroupid = tgid;
     --IF a = 0 THEN
     --   RAISE '#Not test group defined# (%s)', tgid;
     --END IF;
    BEGIN 
        -- Create the new Model
    INSERT INTO isee.board_model (modelid, variant, des, testgroupid) VALUES (modid, var, descr, tgid) RETURNING mid INTO a;
    EXCEPTION WHEN unique_violation THEN
        a := -1;
    END;
    RETURN a;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.create_model(character varying, character varying, character varying, character varying)
  OWNER TO admin;
