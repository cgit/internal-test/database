﻿-- Function: isee.uuid_macaddr(uuid)

-- DROP FUNCTION isee.uuid_macaddr(uuid);

CREATE OR REPLACE FUNCTION isee.uuid_macaddr(id uuid)
  RETURNS macaddr AS
$BODY$
  select substring(id::text, 25, 12)::macaddr
$BODY$
  LANGUAGE sql IMMUTABLE STRICT
  COST 100;
ALTER FUNCTION isee.uuid_macaddr(uuid)
  OWNER TO admin;
