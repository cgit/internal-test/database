﻿-- Function: isee.open_testbatch(uuid)

-- DROP FUNCTION isee.open_testbatch(uuid);

CREATE OR REPLACE FUNCTION isee.open_testbatch(bid uuid)
  RETURNS integer AS
$BODY$
DECLARE
    result integer;
    a integer;
    view_name varchar;
BEGIN
     -- get Next ID
     SELECT MAX(testid_ctl) INTO a FROM isee.test_control;
     IF a IS NULL THEN        
        result := 1;       
     ELSE        
        result := a + 1;
     END IF;
     -- Create new Job
     INSERT INTO isee.test_control (boarduuid, testid_ctl) VALUES (bid, result);
     -- Create View
     -- PERFORM create_view_testbatch (result);
     PERFORM isee.create_temp_table_testbatch_left (bid, result);
     RETURN result;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.open_testbatch(uuid)
  OWNER TO admin;