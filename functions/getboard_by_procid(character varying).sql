﻿-- Function: isee.getboard_by_procid(character varying)

-- DROP FUNCTION isee.getboard_by_procid(character varying);

CREATE OR REPLACE FUNCTION isee.getboard_by_procid(procid character varying)
  RETURNS SETOF isee.board AS
$BODY$
DECLARE
BEGIN
     RETURN QUERY 
     EXECUTE (
             format('SELECT * FROM isee.board WHERE board_processor_id = %s;', quote_literal(procid))
     );
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION isee.getboard_by_procid(character varying)
  OWNER TO admin;
