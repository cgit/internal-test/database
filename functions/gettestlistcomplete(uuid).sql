﻿-- Function: isee.gettestlistcomplete(uuid)

-- DROP FUNCTION isee.gettestlistcomplete(uuid);

CREATE OR REPLACE FUNCTION isee.gettestlistcomplete(IN bid uuid)
  RETURNS TABLE(testdefname character varying, des character varying, testfunc character varying, testparam character varying) AS
$BODY$
DECLARE
    TGroupID varchar;
    mid integer;
  mod_row record;
BEGIN
    TGroupID = isee.gettestgroupid(bid);
    RETURN QUERY 
           select board_test_def.testid, board_test_def.testgroupid, test_definition.testdefname
                  from (isee.board_test_def join isee.test_definition ON 
                  (( isee.board_test_def.testdefid = isee.test_definition.testdefid))) 
                  WHERE isee.board_test_def.testgroupid =TGroupID;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION isee.gettestlistcomplete(uuid)
  OWNER TO admin;
