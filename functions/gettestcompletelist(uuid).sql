﻿-- Function: isee.gettestcompletelist(uuid)

-- DROP FUNCTION isee.gettestcompletelist(uuid);

CREATE OR REPLACE FUNCTION isee.gettestcompletelist(IN bid uuid)
  RETURNS TABLE(testdefname character varying, des character varying, testfunc character varying, testparam character varying) AS
$BODY$
DECLARE
    TGroupID varchar;    
    --mid integer;
    --mod_row record;
BEGIN
    TGroupID = isee.gettestgroupid(bid);    
    RETURN QUERY 
    SELECT test_definition.testdefname, test_definition.des, test_definition.testfunc, board_test_def.testparam 
                  FROM (isee.board_test_def JOIN isee.test_definition ON 
                  (( isee.board_test_def.testdefid = isee.test_definition.testdefid))) 
                  WHERE isee.board_test_def.testgroupid =TGroupID;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION isee.gettestcompletelist(uuid)
  OWNER TO admin;
