﻿-- Function: isee.define_test(character varying, character varying, character varying)

-- DROP FUNCTION isee.define_test(character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION isee.define_test(
    testname character varying,
    testdesc character varying,
    testattr character varying)
  RETURNS integer AS
$BODY$
DECLARE    
    a integer;
BEGIN
     BEGIN
          INSERT INTO isee.test_definition (testdefname, des, testfunc) VALUES (testname, testdesc, testattr) RETURNING testdefid INTO a;     
     EXCEPTION WHEN unique_violation THEN
            a := -1;
     END;     
     return a;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.define_test(character varying, character varying, character varying)
  OWNER TO admin;
