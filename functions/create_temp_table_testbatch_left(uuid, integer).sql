﻿-- Function: isee.create_temp_table_testbatch_left(uuid, integer)

-- DROP FUNCTION isee.create_temp_table_testbatch_left(uuid, integer);

CREATE OR REPLACE FUNCTION isee.create_temp_table_testbatch_left(
    bid uuid,
    temp_table integer)
  RETURNS void AS
$BODY$
DECLARE
    view_name varchar;
    result integer;
BEGIN
     view_name := 'test_left_' || temp_table;
     -- Create Temp table
/*     EXECUTE 'CREATE TEMP TABLE ' || view_name || ' ON COMMIT PRESERVE ROWS AS '
              || ' SELECT testid FROM getTestlist(''' || bid
              || ''');'; */
     EXECUTE 'CREATE TABLE ' || view_name || ' AS '
              || ' SELECT testid FROM isee.gettestlist(''' || bid
              || ''');';                     
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.create_temp_table_testbatch_left(uuid, integer)
  OWNER TO admin;
