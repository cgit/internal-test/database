-- Function: isee.delete_db()

-- DROP FUNCTION isee.delete_db();

CREATE OR REPLACE FUNCTION isee.delete_db()
  RETURNS boolean AS
$BODY$
DECLARE
BEGIN
    DELETE FROM isee.board;
    DELETE FROM isee.board_test;
    DELETE FROM isee.manufacturer_pro;
    DELETE FROM isee.igep_ok;
    DELETE FROM isee.test_control;
    UPDATE isee.set_test SET testid_ctl = NULL;
    UPDATE isee.set_test SET boarduuid = NULL;
    UPDATE isee.set_test SET current_test = NULL;
    UPDATE isee.set_test SET status = NULL;
    -- Now we will reset sequences
    ALTER SEQUENCE isee.board_macaddr_seq RESTART;
    ALTER SEQUENCE isee.board_test_id_seq RESTART;
    ALTER SEQUENCE isee.test_control_testid_ctl_seq RESTART;
    -- Destroy temporal tables if any
    --SELECT 'DROP Table [' + Table_Schema + '].[' + Table_Name + ']' FROM   Information_Schema.Tables WHERE  Table_Type = 'Base Table' AND Table_Schema = 'public';
    RETURN TRUE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.delete_db()
  OWNER TO admin;
