-- Function: isee.update_cpro(character varying)

-- DROP FUNCTION isee.update_cpro(character varying);

CREATE OR REPLACE FUNCTION isee.update_cpro(currentbn character varying)
  RETURNS boolean AS
$BODY$
DECLARE
BEGIN	
	-- First we update current production number
	UPDATE isee.test_defaults SET current_order_of_fabrication = currentbn;
	-- Then we check if the the number is already registered as a batch number, if is do nothing, but if it doesn't then we need to insert it as this will be a new batch to produce
	IF EXISTS (SELECT FROM isee.manufacturer_pro WHERE order_of_fabrication = currentbn) THEN
	
	ELSE
               INSERT INTO isee.manufacturer_pro (order_of_fabrication, actual_ok_count) VALUES (currentbn , '0');
	END IF;
	RETURN TRUE;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.update_cpro(character varying)
  OWNER TO admin;
