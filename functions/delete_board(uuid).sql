-- Function: isee.delete_board(uuid)

-- DROP FUNCTION isee.delete_board(uuid);

CREATE OR REPLACE FUNCTION isee.delete_board(bid uuid)
  RETURNS boolean AS
$BODY$
DECLARE
BEGIN
    IF EXISTS (SELECT FROM isee.board WHERE board_uuid = bid) THEN
    -- Delete igep board from board table
    DELETE FROM isee.board WHERE board_uuid = bid;
    -- Decrement batch number associated with that igep board
    UPDATE isee.manufacturer_pro SET actual_ok_count = isee.manufacturer_pro.actual_ok_count -1 WHERE order_of_fabrication = (SELECT order_of_fabrication FROM isee.igep_ok WHERE board_uuid = bid);
    -- Now we can delete that board from igep_ok table
    DELETE FROM isee.igep_ok WHERE board_uuid = bid;
    RETURN TRUE;
    ELSE
    -- This uuid number does not exist
    RETURN FALSE;
    END IF;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.delete_board(uuid)
  OWNER TO admin;
