﻿-- Function: isee.add_test_to_group(character varying, character varying, character varying)

-- DROP FUNCTION isee.add_test_to_group(character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION isee.add_test_to_group(
    tgid character varying,
    testname character varying,
    parameters character varying)
  RETURNS boolean AS
$BODY$
DECLARE    
    a integer;
BEGIN
     SELECT mid INTO a FROM isee.board_model WHERE testgroupid = tgid;
     IF NOT FOUND THEN
        -- Not Test found
        RETURN FALSE;
     END IF;    
     SELECT testdefid INTO a FROM isee.test_definition WHERE testdefname = testname;
     IF NOT FOUND THEN
        -- Not Test found
        RETURN FALSE;
     END IF;
     BEGIN  
        INSERT INTO isee.board_test_def (testgroupid, testdefid, testparam) VALUES (tgid, a, parameters);
     EXCEPTION WHEN unique_violation THEN
        RETURN FALSE;
     END;
     RETURN TRUE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.add_test_to_group(character varying, character varying, character varying)
  OWNER TO admin;
