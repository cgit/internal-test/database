﻿-- Function: isee.test_func(uuid, integer, integer)

-- DROP FUNCTION isee.test_func(uuid, integer, integer);

CREATE OR REPLACE FUNCTION isee.test_func(
    bid uuid,
    exec_id integer,
    idid integer)
  RETURNS character varying AS
$BODY$
DECLARE
    a integer;
    t character varying;
    t_id integer;
    pr varchar;
    
BEGIN
     -- Remove this test test from test list
     t := 'SELECT COUNT(*) FROM  test_left_' || idid;
  EXECUTE t INTO a;
     
     --SELECT COUNT(*) INTO pr FROM test_left_5;
     -- Check if we've more test to do
     --IF a = 0 THEN
      -- Not have more test to do
    --    PERFORM isee.update_testbatch(bid, exec_id, 'TERMINATED');
     --ELSE
      -- left more test
      --  PERFORM isee.update_testbatch(bid, exec_id, 'RUN');
     --END IF;
     RETURN a;              
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.test_func(uuid, integer, integer)
  OWNER TO admin;