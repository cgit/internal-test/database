﻿-- Function: isee.add_test_to_batch_c(uuid, character varying, integer, boolean, character varying, character varying)

-- DROP FUNCTION isee.add_test_to_batch_c(uuid, character varying, integer, boolean, character varying, character varying);

CREATE OR REPLACE FUNCTION isee.add_test_to_batch_c(
    bid uuid,
    test_name character varying,
    exec_id integer,
    res boolean,
    groupid character varying,
    dat character varying)
  RETURNS boolean AS
$BODY$
DECLARE
    a integer;
    t varchar;
    t_num integer;
    t_id integer;
    
BEGIN
     -- Remove this test test from test list
     t := 'test_left_' || exec_id;
     SELECT testdefid INTO t_num FROM isee.test_definition WHERE testdefname = test_name;
     SELECT testid INTO t_id FROM isee.board_test_def WHERE testdefid = t_num  AND testgroupid = groupid;   
     EXECUTE 'DELETE FROM ' || t || ' WHERE testid = ' || t_id;     
     -- INSERT Test Result
     INSERT INTO isee.board_test (testid, board_uuid, result, external_data, testid_ctl) 
            VALUES (t_id, bid, res, dat, exec_id);
     -- Count number of test remain in the list
     EXECUTE 'SELECT COUNT(*) FROM ' || t INTO a;
     -- Check if we've more test to do
     IF a = 0 THEN
        -- Not have more test to do
        PERFORM isee.update_testbatch(bid, exec_id, 'TERMINATED');
     ELSE
        -- left more test
        PERFORM isee.update_testbatch(bid, exec_id, 'RUN');
     END IF;
     RETURN true;              
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.add_test_to_batch_c(uuid, character varying, integer, boolean, character varying, character varying)
  OWNER TO admin;
