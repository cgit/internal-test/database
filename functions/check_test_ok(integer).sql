﻿-- Function: isee.check_test_ok(integer)

-- DROP FUNCTION isee.check_test_ok(integer);

CREATE OR REPLACE FUNCTION isee.check_test_ok(test_ctl_id integer)
  RETURNS boolean AS
$BODY$
DECLARE
    -- view_name varchar;
    a varchar[];  
    t character varying;
    c record;
BEGIN
     FOR c IN SELECT * FROM isee.board_test WHERE testid_ctl = test_ctl_id LOOP
         IF c.result = FALSE THEN
    t := 'SELECT ARRAY (SELECT testdefname FROM isee.test_definition WHERE testdefid IN
    (SELECT testdefid FROM isee.board_test_def WHERE testid IN 
    (SELECT testid FROM isee.board_test WHERE (testid_ctl = ' || test_ctl_id || ' AND result = FALSE))))';
    EXECUTE t INTO a;
    UPDATE isee.set_test SET current_test = a WHERE testid_ctl = test_ctl_id; 
          -- We've a test failed
            RETURN FALSE;
         END IF;
     END LOOP;
     -- Test OK
     RETURN TRUE;                   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.check_test_ok(integer)
  OWNER TO admin;
