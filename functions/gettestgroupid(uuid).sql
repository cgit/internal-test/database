﻿-- Function: isee.gettestgroupid(uuid)

-- DROP FUNCTION isee.gettestgroupid(uuid);

CREATE OR REPLACE FUNCTION isee.gettestgroupid(bid uuid)
  RETURNS character varying AS
$BODY$
DECLARE
    TGroupID varchar;
    mod_id integer;
  mod_row record;
BEGIN
     TGroupID = 'none';
     SELECT mid INTO mod_id FROM isee.board WHERE board_uuid = bid;
     IF FOUND THEN
        SELECT isee.board_model.testgroupid INTO TGroupID FROM isee.board_model WHERE isee.board_model.mid = mod_id;
        IF NOT FOUND THEN
           RAISE '#Board have not Test assigned# (%)', bid;
        END IF;         
     ELSE
         RAISE '#Board not found# (%)', bid;       
     END IF;
     RETURN TGroupID;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.gettestgroupid(uuid)
  OWNER TO admin;
