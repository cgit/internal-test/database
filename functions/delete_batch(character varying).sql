-- Function: isee.delete_batch(character varying)

-- DROP FUNCTION isee.delete_batch(character varying);

CREATE OR REPLACE FUNCTION isee.delete_batch(oof character varying)
  RETURNS boolean AS
$BODY$
DECLARE
BEGIN
    IF EXISTS (SELECT FROM isee.manufacturer_pro WHERE order_of_fabrication = oof) THEN
	-- Delete from board table all igeps that corresponds to that batch number
	DELETE FROM isee.board WHERE board_uuid = (SELECT board_uuid FROM isee.igep_ok WHERE order_of_fabrication = oof);
	-- Delete from igep_ok table all igeps that corresponds to that batch bynber
	DELETE FROM isee.igep_ok WHERE order_of_fabrication = oof;
	-- Now we can delete that batch number from manufacturer_pro table
	DELETE FROM isee.manufacturer_pro WHERE order_of_fabrication = oof;
	RETURN TRUE;
    ELSE
	-- This batch number does not exist
	RETURN FALSE;
    END IF;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.delete_batch(character varying)
  OWNER TO admin;
