﻿-- Function: isee.create_board(character varying, character varying, character varying, macaddr)

-- DROP FUNCTION isee.create_board(character varying, character varying, character varying, macaddr);

CREATE OR REPLACE FUNCTION isee.create_board(
    procid character varying,
    modid character varying,
    var character varying,
    bmac_ext macaddr)
  RETURNS uuid AS
$BODY$
DECLARE
  res uuid;
  mod_row record;
  macaux macaddr;
BEGIN
  res = NULL;
  
  -- Check if board exist
  IF EXISTS (SELECT FROM isee.board WHERE board_processor_id = procid) THEN
    -- Check if is marked as recyclable
    --IF (SELECT recyclable FROM isee.board WHERE board_processor_id = procid) = TRUE THEN
      -- if true, mac will be rehused also board_creation time shouldn't be rewritten, so only mark recyclable as FALSE again, board is fine now
      --UPDATE isee.board SET board_creation = now() WHERE board_processor_id = procid;
    --  UPDATE isee.board SET recyclable = False WHERE board_processor_id = procid;
    --ELSE
      -- if false do nothing, board already created and not recyclable, board is fine
    --END IF;
  -- If board exists we will return uuid in res variable
  SELECT board_uuid INTO res FROM isee.board WHERE board_processor_id = procid;
  ELSE
    -- Check if there is any rehusable board
    --IF EXISTS (SELECT FROM isee.board WHERE recyclable = True) THEN
      -- if rehusable board found, update all the fields but not macaddr because it should be rehused, also mark recyclable as FALSE, board is fine now
      -- Obtain macaddr to reupdate
    --  SELECT macaddr INTO macaux FROM isee.board WHERE recyclable = True ORDER BY board_creation limit 1;
      -- Obtain Model information from Models table
    --  SELECT INTO mod_row * FROM isee.board_model WHERE ( modelid = modid) AND ( variant = var);
    --  IF FOUND THEN
        -- Generate new uuid
    --    res = uuid_generate_v1();
        -- Update all fields
    --    UPDATE isee.board SET board_uuid = res WHERE macaddr = macaux;
    --    UPDATE isee.board SET board_processor_id = procid WHERE macaddr = macaux;
    --    UPDATE isee.board SET board_creation = now() WHERE macaddr = macaux;    
    --    UPDATE isee.board SET mid = mod_row.mid WHERE macaddr = macaux;
    --    UPDATE isee.board SET recyclable = False WHERE macaddr = macaux;
    --  ELSE
        -- No Valid Model Found  Raise Exception
    --    RAISE EXCEPTION '#Model not exist# (%,%,%)', modid, pcbid, assid;
    --  END IF;   
    --ELSE
    -- if no rehusable board found insert into the table with new macaddr based on count and mark recyclabel as FALSE, board is fine now
    -- Obtain Model information from Models table
    SELECT INTO mod_row * FROM isee.board_model WHERE ( modelid = modid) AND ( variant = var);
    -- Generate new uuid
    res = uuid_generate_v1();
    -- Check if macaddr is NULL (external macaddr can be provided
    IF bmac_ext is NULL THEN
      -- We generate the address
      -- ((default macaddr in bigint + count of total rows in bigint) this to hex(text) add a 0 to the left side) this casted to macaddr !!
      --OPTION 1 USE COUNT TO OBTAIN NEXT IN SEQUENCE, DO NOT REMOVE ROW OR IT WILL BUG !
      --macaux = (SELECT (SELECT CONCAT('0',(SELECT hex(((SELECT x'020000000000'::bigint) + (SELECT COUNT(*) FROM isee.board))))))::macaddr);
      -- OPTION 2 USE SEQUENCE TO OBTAIN NEXT IN SEQUENCE
      --nextval('isee.board_test_def_testid_seq'::regclass)
      --macaux = (SELECT (SELECT CONCAT('0',(SELECT hex(((SELECT x'020000000000'::bigint) + (SELECT nextval('isee.board_macaddr_seq'::regclass)))))))::macaddr);
      --SELECT ('x'|| (SELECT macaddr_text FROM isee.test_defaults))::bit(48)::bigint;
      macaux = (SELECT (SELECT CONCAT('0',(SELECT hex(((SELECT ('x'|| (SELECT starting_macaddr FROM isee.test_defaults))::bit(48)::bigint) + (SELECT nextval('isee.board_macaddr_seq'::regclass)))))))::macaddr);
    ELSE
      -- ELSE get the macaddr from bmac_ext
      macaux = bmac_ext; 
    END IF;
    -- INSERT THE BOARD with all the data
    BEGIN
      INSERT INTO isee.board (board_uuid, board_processor_id, bmac0, mid) VALUES ( res , procid, macaux, mod_row.mid);
    EXCEPTION WHEN unique_violation THEN
      RAISE '#TTTTT#  asfsfd';
    END;    
    --END IF; from rehusable IF clause
  END IF;
  -- Return uuid for this board
  RETURN res;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.create_board(character varying, character varying, character varying, macaddr)
  OWNER TO admin;
