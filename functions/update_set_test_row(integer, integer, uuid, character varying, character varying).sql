﻿-- Function: isee.update_set_test_row(integer, integer, uuid, character varying, character varying)

-- DROP FUNCTION isee.update_set_test_row(integer, integer, uuid, character varying, character varying);

CREATE OR REPLACE FUNCTION isee.update_set_test_row(
    nstation integer,
    tbid integer,
    bid uuid,
    ctest character varying,
    final_st character varying)
  RETURNS void AS
$BODY$
DECLARE
BEGIN
  IF final_st = 'FINISH' THEN
      UPDATE isee.set_test SET testid_ctl = tbid, boarduuid = bid, current_test = ctest WHERE station = nstation;
  ELSE
  UPDATE isee.set_test SET testid_ctl = tbid, boarduuid = bid, current_test = ctest, status = final_st WHERE station = nstation;
  END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.update_set_test_row(integer, integer, uuid, character varying, character varying)
  OWNER TO admin;
