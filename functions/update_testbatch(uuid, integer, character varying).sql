﻿-- Function: isee.update_testbatch(uuid, integer, character varying)

-- DROP FUNCTION isee.update_testbatch(uuid, integer, character varying);

CREATE OR REPLACE FUNCTION isee.update_testbatch(
    bid uuid,
    tbid integer,
    new_st character varying)
  RETURNS void AS
$BODY$
DECLARE
BEGIN
     IF new_st = 'TERMINATED' THEN
         UPDATE isee.test_control SET status = new_st, complete = True WHERE boarduuid = bid AND testid_ctl = tbid;
     ELSE
         UPDATE isee.test_control SET status = new_st, complete = False WHERE boarduuid = bid AND testid_ctl = tbid;
     END IF;     
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.update_testbatch(uuid, integer, character varying)
  OWNER TO admin;
