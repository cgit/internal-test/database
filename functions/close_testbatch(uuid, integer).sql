﻿-- Function: isee.close_testbatch(uuid, integer)

-- DROP FUNCTION isee.close_testbatch(uuid, integer);

CREATE OR REPLACE FUNCTION isee.close_testbatch(
    bid uuid,
    tbid integer)
  RETURNS boolean AS
$BODY$
DECLARE
    t record;
    result boolean;
    view_name varchar;
BEGIN
     -- Verify if the board and test exist in the control table
     SELECT INTO t * FROM isee.test_control WHERE boarduuid = bid AND testid_ctl = tbid;
     IF NOT FOUND THEN
        -- IF Test or board Not Found then Exit Now
        RAISE '#TEST not found# (%)', tbid;
     END IF;
     -- IF Found check the Test status and if the test is complete
     IF (t.status <> 'TERMINATED') AND (t.complete = TRUE) THEN
        -- IF Test is Not complete then it's not possible
        -- close this Test
        RAISE '#Test not complete# (%)', tbid;
     END IF;
     -- Verify if the Test is OK 
     result := isee.check_test_ok(tbid);
     IF result = FALSE THEN
        -- Test Failed
        UPDATE isee.test_control SET status = 'TEST_FAILED' WHERE testid_ctl = tbid;
        UPDATE isee.test_control SET complete = 'FALSE' WHERE testid_ctl = tbid;
        UPDATE isee.set_test SET status = 'TEST_FAILED' WHERE testid_ctl = tbid;
        -- Destroy Table with Test cases left     
    view_name := 'test_left_' || tbid;
    EXECUTE 'DROP TABLE IF EXISTS ' || view_name;
    -- Test Fail
    RETURN FALSE;
        --RAISE '#TEST Failed# (%)', tbid;
     ELSE
     UPDATE isee.test_control SET status = 'TEST_COMPLETE' WHERE testid_ctl = tbid;
         UPDATE isee.test_control SET complete = 'TRUE' WHERE testid_ctl = tbid;
         UPDATE isee.set_test SET status = 'TEST_COMPLETE' WHERE testid_ctl = tbid;
     -- get variables from other tables
     -- fill igep_ok board
     IF EXISTS (SELECT FROM isee.igep_ok WHERE board_uuid = bid) THEN
        -- If a board already passed the test with OK then it will be found in igep_ok
        -- therefore we will do nothing because it is expected that nothing changed, also we avoid putting same board in two different manufacturer order of fabrication doing an erroneos UPDATE
        -- BOARD UUID EXIST, UPDATE ROW
        --UPDATE isee.igep_ok SET board_pid = (SELECT board_processor_id FROM isee.board WHERE board_uuid = bid) WHERE isee.igep_ok.board_uuid = bid;
        --UPDATE isee.igep_ok SET model = (SELECT modelid FROM (isee.board JOIN isee.board_model ON ((isee.board.mid = isee.board_model.mid))) WHERE isee.board.board_uuid = bid) WHERE isee.igep_ok.board_uuid = bid;
        --UPDATE isee.igep_ok SET variant = (SELECT variant FROM (isee.board JOIN isee.board_model ON ((isee.board.mid = isee.board_model.mid))) WHERE isee.board.board_uuid = bid) WHERE isee.igep_ok.board_uuid = bid;
        -- It is suposed that board manufacturer is set up when we deploy the entire DB in manufacturers host
        --UPDATE isee.igep_ok SET board_manufacturer = (SELECT board_manufacturer FROM isee.manufacturer) WHERE isee.igep_ok.board_uuid = bid;
        -- It is suposed that isee current order of fabrication is up to date thanks to nodejs setting it before we get here;
        --UPDATE isee.igep_ok SET order_of_fabrication = (SELECT current_order_of_fabrication FROM isee.test_defaults) WHERE isee.igep_ok.board_uuid = bid;
        --UPDATE isee.igep_ok SET manf_timestamp = (SELECT date_trunc('second',(SELECT board_creation FROM isee.board WHERE board_uuid = bid))::timestamp(0)) WHERE isee.igep_ok.board_uuid = bid;
        --UPDATE isee.igep_ok SET bmac0 = (SELECT bmac0 FROM isee.board WHERE board_uuid = bid) WHERE isee.igep_ok.board_uuid = bid;
        --UPDATE isee.igep_ok SET bmac1 = (SELECT ('02:00:00:00:00:00')::macaddr) WHERE isee.igep_ok.board_uuid = bid;
     ELSE
        -- BOARD UUID DON'T EXIST, CREATE ROW
        --INSERT INTO isee.igep_ok (board_uuid, board_pid, model, variant, board_manufacturer, order_of_fabrication, manf_timestamp, bmac0, bmac1)
        INSERT INTO isee.igep_ok (board_uuid, board_pid, model, variant, order_of_fabrication, manf_timestamp, bmac0, bmac1)
        VALUES (
        bid, 
        (SELECT board_processor_id FROM isee.board WHERE board_uuid = bid), 
        (SELECT modelid FROM (isee.board JOIN isee.board_model ON ((isee.board.mid = isee.board_model.mid))) WHERE isee.board.board_uuid = bid), 
        (SELECT variant FROM (isee.board JOIN isee.board_model ON ((isee.board.mid = isee.board_model.mid))) WHERE isee.board.board_uuid = bid), 
        -- It is suposed that board manufacturer is set up when we deploy the entire DB in manufacturers host
        --(SELECT board_manufacturer FROM isee.manufacturer),
        -- It is suposed that isee current order of fabrication is up to date thanks to nodejs setting it before we get here; 
        (SELECT current_order_of_fabrication FROM isee.test_defaults), 
        (SELECT date_trunc('second',(SELECT board_creation FROM isee.board WHERE board_uuid = bid))::timestamp(0)), 
        (SELECT bmac0 FROM isee.board WHERE board_uuid = bid), (SELECT ('02:00:00:00:00:00')::macaddr));
        -- Increase counter of igep ok boards we need to avoid the bug of launching test without nodejs
        -- If we launch it this way, there is no new batch number created so we need to created it first
        IF EXISTS (SELECT FROM isee.manufacturer_pro WHERE order_of_fabrication = (SELECT current_order_of_fabrication FROM isee.test_defaults)) THEN
            UPDATE isee.manufacturer_pro SET actual_ok_count = isee.manufacturer_pro.actual_ok_count + 1 WHERE order_of_fabrication = (SELECT current_order_of_fabrication FROM isee.test_defaults);
        ELSE
            INSERT INTO isee.manufacturer_pro (order_of_fabrication, actual_ok_count)
            VALUES (
            (SELECT current_order_of_fabrication FROM isee.test_defaults),
            '1');
        END IF;
     END IF;
     END IF;         
     -- Destroy Table with Test cases left     
     view_name := 'test_left_' || tbid;
     EXECUTE 'DROP TABLE IF EXISTS ' || view_name;
     -- Test OK
     RETURN TRUE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION isee.close_testbatch(uuid, integer)
  OWNER TO admin;
